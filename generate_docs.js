/**
 * Created by James on 1/13/14.
 * Nothing fancy. Just a simple Node.js script responsible for using doxx generator to produce documentaiton based off of
 * javascript comments.
 */
var exec = require('child_process').exec;
var srcDir = __dirname + '/lib',
    docsDir = __dirname + '/docs',
    adaptersDir = __dirname + '/lib/adapters',
    adaptersDocsDir = __dirname + '/docs/adapters';

// generate documentation using DOXX generator.
console.log('Generating vanguard-core source documentation...');

exec('doxx --source ' + srcDir + ' --target ' + docsDir + ' -i bin,docs,template,node_modules,public,routes,tests,views,adapters -t vanguard-core', function(err, stderr,stdout) {
    if (err) console.log(err);
    if (stderr) console.log(stderr);
    if (stdout) console.log(stdout);
    console.log('Documentation generated for vanguare-core base library routines.');
});

exec('doxx --source ' + adaptersDir + ' --target ' + adaptersDocsDir + ' -i bin,docs,template,node_modules,public,routes,tests,views -t vanguard-core-adapters', function(err, stderr,stdout) {
    if (err) console.log(err);
    if (stderr) console.log(stderr);
    if (stdout) console.log(stdout);
    console.log('Documentation generated for vanguard-core adapters.');
});