/*
* Generate Model based off of DB Schema
* -------------------------------------
* 
* 
* 
* 
*/

global.vg = require(require('path').resolve('./node_modules/vanguard-core/lib/core'));
vg.config = require(require('path').resolve('./environments/' + process.argv[2]));

if (process.argv.length <= 2) {
    // Display Help
    console.log("Vanguard Model Generator");
    console.log("------------------------");
    console.log("Example:");
    console.log("     node build_model database table_name model_name");
    console.log("");
} else {
    var database = process.argv[3];
    var table_name = process.argv[4];
    var model_name = process.argv[5];

    console.log('Generating Model ...');
    vg.db.getAdapter(database).buildModel(model_name,database,table_name, function(err, result){
        if (err) {
            console.log(err);
        } else {
            console.log('Model Generated: ' + __dirname + '/' + model_name + '_model.js');
        }
    });
}