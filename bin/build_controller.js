/*
 * Generate Model based off of DB Schema
 * -------------------------------------
 *
 *
 *
 *
 */

global.vg = require('./node_modules/vanguard-core/lib/core');
vg.config = require('./environments/' + process.argv[2]);

if (process.argv.length <= 2) {
    // Display Help
    console.log("Vanguard Controller Generator");
    console.log("------------------------");
    console.log("Example:");
    console.log("     node build_controller database table_name model_name");
    console.log("");
} else {
    var database = process.argv[3];
    var table_name = process.argv[4];
    var model_name = process.argv[5];

    console.log('Generating Controller ...');
    vg.db.getAdapter(database).buildController(model_name,database,table_name, function(err, result){
        if (err) {
            console.log(err);
        } else {
            console.log('Controller Generated: ' + __dirname + '/' + model_name + '_controller.js');
        }
    });
}