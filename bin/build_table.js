/*
 * Generate Table based off of Model
 * -------------------------------------
 *
 *
 *
 *
 */
var vgApp = require('./node_modules/vanguard-core/app.js');
global.vg = require('./node_modules/vanguard-core/lib/core');
vg.config = require('./environments/' + process.argv[2]);
vg.disableWebServer = true;
vgApp.initialize(vg.config);

if (process.argv.length <= 2) {
    // Display Help
    console.log("Vanguard Table Generator");
    console.log("------------------------");
    console.log("Example:");
    console.log("     node build_table database model_name");
    console.log("");
} else {
    var database = process.argv[3];
    var model_name = process.argv[4];

    console.log('Generating Table ...');
    vg.db.getAdapter(database).sync(vg.db[database].models[model_name],{callback: function(err, result){
        if (err) {
            console.log(err);
        } else {
            console.log('Table Generated!');
        }
    }});
}