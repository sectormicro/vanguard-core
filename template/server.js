var vgApp = require('./node_modules/vanguard-core/app.js');

vgApp.initialize(require('./environments/' + process.argv[2]));

require('./app/utils');

// Setup Basic Ruleset

vg.config.rules = vg.config.rules ? vg.config.rules : {};
vg.config.rules.password_minimum_length = vg.config.rules.password_minimum_length ? vg.config.rules.password_minimum_length : 2 ;
vg.config.rules.password_maximum_length = vg.config.rules.password_maximum_length ? vg.config.rules.password_maximum_length : 8 ;