if (typeof window === 'undefined') {
    var window = global;
    var browser = false;
} else {
    var browser = true;
}
window.app = window.app ? window.app : {};
window.vg = window.vg ? window.vg : {};
app.utils = app.utils ? app.utils : {};
if (browser) {
    window.vg.utils = app.utils;
    _.mixin(_.str);
    console.log('Window Utility Mode Loaded');
} else {
    global.CryptoJS = require('cryptojs').Crypto;
    _.str = require('underscore.string');
    _.mixin(_.str.exports());
    _.str.include('Underscore.string', 'string');
    global.moment = require('moment');
}


(function () {

    //
    // Iterates over an array of numbers and returns the sum. Example:
    //
    //    _.sum([1, 2, 3]) => 6
    //
    _.sum = function (obj, field) {
        var array = _.pluck(obj, field);
        //if (!$.isArray(obj) || obj.length == 0) return 0;
        return _.reduce(array, function (sum, n) {
            return sum += n;
        });
    }

})();

vg.utils.get_business_days = function (dDate1, dDate2) {

    var iWeeks, iDateDiff, iAdjust = 0;

    if (dDate2 < dDate1) return -1;                 // error code if dates transposed

    var iWeekday1 = dDate1.getDay();                // day of week
    var iWeekday2 = dDate2.getDay();

    iWeekday1 = (iWeekday1 == 0) ? 7 : iWeekday1;   // change Sunday from 0 to 7
    iWeekday2 = (iWeekday2 == 0) ? 7 : iWeekday2;

    if ((iWeekday1 > 5) && (iWeekday2 > 5)) iAdjust = 1;  // adjustment if both days on weekend

    iWeekday1 = (iWeekday1 > 5) ? 5 : iWeekday1;    // only count weekdays
    iWeekday2 = (iWeekday2 > 5) ? 5 : iWeekday2;

    // calculate differnece in weeks (1000mS * 60sec * 60min * 24hrs * 7 days = 604800000)
    iWeeks = Math.floor((dDate2.getTime() - dDate1.getTime()) / 604800000)

    if (iWeekday1 <= iWeekday2) {
        iDateDiff = (iWeeks * 5) + (iWeekday2 - iWeekday1)
    } else {
        iDateDiff = ((iWeeks + 1) * 5) - (iWeekday1 - iWeekday2)
    }

    iDateDiff -= iAdjust                            // take into account both days on weekend

    return (iDateDiff + 1);                         // add 1 because dates are inclusive
};
//_.mixin(_.str.exports());

// Primitive Prototypes
// --------------------
if (!String.prototype.startsWith) {
    Object.defineProperty(String.prototype, 'startsWith', {
        enumerable: false,
        configurable: false,
        writable: false,
        value: function (searchString, position) {
            position = position || 0;
            return this.indexOf(searchString, position) === position;
        }
    });
}
String.prototype.replaceAll = function (target, replacement) {
    return this.split(target).join(replacement);
};

// the date format prototype
Date.prototype.format = function (f) {


// a global month names array
    var gsMonthNames = new Array(
        'January',
        'February',
        'March',
        'April',
        'May',
        'June',
        'July',
        'August',
        'September',
        'October',
        'November',
        'December'
    );
// a global day names array
    var gsDayNames = new Array(
        'Sunday',
        'Monday',
        'Tuesday',
        'Wednesday',
        'Thursday',
        'Friday',
        'Saturday'
    );
    if (!this.valueOf())
        return '&nbsp;';

    var d = this;

    return f.replace(/(yyyy|mmmm|mmm|mm|dddd|ddd|dd|hh|nn|ss|a\/p)/gi,
        function ($1) {
            switch ($1.toLowerCase()) {
                case 'yyyy':
                    return d.getFullYear();
                case 'mmmm':
                    return gsMonthNames[d.getMonth()];
                case 'mmm':
                    return gsMonthNames[d.getMonth()].substr(0, 3);
                case 'mm':
                    return (d.getMonth() + 1);
                case 'dddd':
                    return gsDayNames[d.getDay()];
                case 'ddd':
                    return gsDayNames[d.getDay()].substr(0, 3);
                case 'dd':
                    return d.getDate();
                case 'hh':
                    return ((h = d.getHours() % 12) ? h : 12);
                case 'nn':
                    return d.getMinutes();
                case 'ss':
                    return d.getSeconds();
                case 'a/p':
                    return d.getHours() < 12 ? 'a' : 'p';
            }
        }
    );
}


// Angular App Enhancements [exposed in site scope]
// ------------------------------------------------


String.prototype.valid_cc = function () {
    //Credit - https://sites.google.com/site/abapexamples/javascript/luhn-validation
    var luhnArr = [[0,2,4,6,8,1,3,5,7,9],[0,1,2,3,4,5,6,7,8,9]], sum = 0;
    this.replace(/\D+/g,"").replace(/[\d]/g, function(c, p, o){
        sum += luhnArr[ (o.length-p)&1 ][ parseInt(c,10) ];
    });
    return (sum%10 === 0) && (sum > 0);
}


