var email = require("emailjs");
var server = email.server.connect(vg.config.smtp);

module.exports = function (database, schema) {

    var worker = {};
    worker.name = "Email";

    worker.db = database;

    worker.renderTemplate = function (template, data) {
        var template = fs.readFileSync('app/templates/' + template + '.mustache', 'utf-8');
        var general = fs.readFileSync('app/templates/general.mustache', 'utf-8');
        data.today = new Date();
        data.today_formatted = (new Date()).format('MM/dd/yyyy');
        data.support_phone = vg.config.support_phone ? vg.config.support_phone : '<Phone Here>';
        data.support_email = vg.config.support_email ? vg.config.support_email : '<Email Here>';

        var content = Mustache.render(template, data);
        data.content = content;return Mustache.render(general, data);

    }

    worker.send = function (settings, cb) {

        if (settings.template) {
            settings.attachment = settings.attachment ? settings.attachment : [];
            settings.attachment.push({data: worker.renderTemplate(settings.template, settings.data), alternative: true});
        }

        server.send(settings, cb);

    };

    return worker;
};
global.vg.email = module.exports;