// Begin Test Logic
var should = require('should');

describe('Save Collection', function () {
    it('Should build a list of users and save them all. Then delete all the new users', function (done2) {
        var users = [];

        var user1 = vg.db[vg.test_db].models.User.create({});
        user1.id = 'TEST1';

        var user2 = vg.db[vg.test_db].models.User.create({});
        user2.id = 'TEST2';

        users.push(user1);
        users.push(user2);


        vg.db.getAdapter(vg.test_db).openConnection(vg.db[vg.test_db], function (err, conn) {
            vg.db.getAdapter(vg.test_db).saveCollection(users, {connection: conn, model: vg.db[vg.test_db].models.User}, function (err, results) {
                should.exist(results);
                should.not.exist(err);
                conn.queryRaw('delete from [user] where recnum in (' + _.pluck(results, 'recnum').toString() + ')',function (err) {
                    should.not.exist(err);
                    done2();
                });
            });

        });
    });
})