// Begin Test Logic
var should = require('should');

describe('Query Batch', function () {
    it('.Run', function (done2) {

        vg.db.getAdapter(vg.test_db).openConnection(vg.db[vg.test_db], function (err, conn) {
            vg.db.getAdapter(vg.test_db).queryBatch('select * from [user]; select * from agent;', {models: [vg.db[vg.test_db].models.User, vg.db[vg.test_db].models.Agent], connection: conn}, function (err, set) {
                should.exist(set);
                should.not.exist(err);

                done2();
            });

        });
    });
})