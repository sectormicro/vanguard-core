var tests = [
			{name: 'mstds', description: 'Run Basic Database Tests for Microsoft SQL Server and FreeTDS'},
			{name: 'mysql', description: 'Run Basic Database Tests for MySQL'},
		];
module.exports = {
	
	index: function(req, res) {
		// List Available Tests
		res.send(tests);
	},

	run: function(req, res) {

		var result = {errors: []};

		result.test = _.where(tests, {name: req.params.name}).length > 0 ? _.where(tests, {name: req.params.name})[0] : null;

		if (result.test && fs.existsSync('./node_modules/vanguard-core/tests/' + req.params.name + '.js')) {
			exec('node run_tests ' + req.params.name + ' mocha-html-reporter > "test_' + req.params.name + '.txt', function(err, stderr, stdout){
				result.content = fs.readFileSync('test_' + req.params.name + '.txt', 'utf-8');
				res.send(result);
			});
		} else {
			result.errors.push('Unknown Test [./node_modules/vanguard-core/tests/' + req.params.name + ']');
			res.send(result);
		}


	}

}