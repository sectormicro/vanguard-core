/**
 * Created by Controller Generation Script on Fri Oct 04 2013 11:53:51 GMT-0400 (Eastern Daylight Time).
 */
module.exports = {

    // @initial(req, res)
    // -----------------
    // Builds and sends initial data cache
    initial: function (req, res) {

        flow.exec(

            function () {
                this();
                // Do Async Cache Stuff here!
            }
            , function (err, results) {

                var active_user = _.clone(req.session.user);
                delete active_user.passwd;

                res.send({

                    'active_user': active_user // Active user is returned in the cache
                });
            }
        );

    }

}