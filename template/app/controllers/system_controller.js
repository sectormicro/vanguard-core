module.exports = {

    utils: function(req, res) {
        res.sendfile('./app/utils.js');
    },

    download_updates: function(req, res) {
        // This eventually will be used to pull down latest updates from git
    },

    restart_service: function(req, res) {
        var exec = require('child_process').exec;
        if (vg.config.service_name) {
            res.send({});
        } else {
            res.send({errors: ['Not installed as service!']});
        }
    },

    encrypt: function(req, res){
        var result = {errors: []};
        var leave = function(err){
            if (err)
                result.errors.push(err.toString());
            res.send(result);
        }
        result.value = CryptoJS.AES.encrypt(req.body.value, vg.config.encryption_key);
        leave();
    },

    decrypt: function(req, res){
        var result = {errors: []};
        var leave = function(err){
            if (err)
                result.errors.push(err.toString());
            res.send(result);
        }
        result.value = CryptoJS.AES.decrypt(req.body.value, vg.config.encryption_key);
        leave();
    },

    // @databases
    // ----------
    // Returns a payload containing a list[databases] of databases available to the user
    databases: function (req, res) {
        var result = {databases: []};
        _.each(_.pairs(vg.config.databases), function(pair){
            var db = _.clone(pair[1]);
            db.id = pair[0];
            delete db.connection_string;
            result.databases.push(db);
        });
        res.send(result);
    },

    // @authenticate
    // -------------
    // Called to check if specified user information is correct. If so it adds it to the current session and returns back to the client
    // Expected request.body = {
    //     database: <string>,
    //     username: <string>,
    //     password: <string>
    // }
    authenticate: function (req, res) {
        var result = {errors: []};
        var db = req.body.database;
        req.body.password = req.body.password ? req.body.password : '';
        req.body.username = req.body.username ? req.body.username : '';
        if (req.body.username === 'admin' && req.body.password === 'password'){
            req.session.user = req.body;
            req.session.database = db;
            res.send(result); // Success
        } else {
            res.send({errors: ['Invalid Credentials']});
        }
    },

    // @deauthenticate
    // ---------------
    // Removes user's current session.user information and redirects them to login page
    deauthenticate: function(req, res) {
        delete req.session.user;
        req.session.destroy();
        req.session = null;
        res.send({login_url: vg.config.login_url ? vg.config.login_url : '/login.html'});
    },

}