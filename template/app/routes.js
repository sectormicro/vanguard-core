require('../node_modules/vanguard-core/lib/base_router');

/*

 Sample Routing:
 --------------

 // Basic Route
 // -----------
 vg.app.router.get('/', function(req, res) {
 res.send('Hello World');
 });

 // Controlled Route
 // ----------------
 // Looks for a users controller and then calls the index action.
 // Note if controllers contain a before function then the before route
 // will be reached first.

 vg.app.routing.get('/api/users', 'users.index', {
 check_for_user: true,   // When enabled checks the session for a property (default: user)
 user_property: 'user',  // When specified checks the session for the specified property
 success: function(req, res, strategy, callback) {
 console.log('Checking Role Security');
 callback(req,res);
 }
 });

 */

vg.app.routing.before_all = function (req, res, next) {
    var authenticated = false;

    if (req.session && req.session.user) {
        authenticated = true;
    }

    if (!authenticated && !(req.url.toLowerCase() === '/login.html'
        || req.url.toLowerCase().indexOf('.js') > -1
        || req.url.toLowerCase().indexOf('/api/') > -1
        || req.url.toLowerCase().indexOf('.css') > -1
        || req.url.toLowerCase().indexOf('/img/') > -1)) {
        // The above routes never require authentication
        res.redirect(vg.config.login_url ? vg.config.login_url : '/login.html')
    } else {
        next();
    }
}
/*
vg.app.routing.get('/api/users', 'users.index', {
    check_for_user: false,   // When enabled checks the session for a property (default: user)
    user_property: 'user',  // When specified checks the session for the specified property
    success: function (req, res, strategy, callback) {
        callback(req, res);
    }
});
*/
vg.app.routing.post('/api/system/authenticate', 'system.authenticate', false);
vg.app.routing.post('/api/system/deauthenticate', 'system.deauthenticate', true);
vg.app.routing.get('/api/system/databases', 'system.databases', false);
vg.app.routing.get('/api/system/utils.js', 'system.utils', false);
vg.app.routing.put('/api/system/encrypt', 'system.encrypt', true);
vg.app.routing.put('/api/system/decrypt', 'system.decrypt', true);
vg.app.routing.get('/api/system/restart_service', 'system.restart_service', true);

vg.app.routing.get('/api/tests', 'tests.index', true);
vg.app.routing.get('/api/tests/run/:name', 'tests.run', true);
vg.app.routing.get('/api/cache/initial', 'cache.initial', true);