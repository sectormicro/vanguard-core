'use strict';

/* Directives */


var mod = angular.module('directives', [])
    .directive('appVersion', ['version', function (version) {
        return function (scope, elm, attrs) {
            elm.text(version);
        };
    }])
    .directive('ngBlur', function() {
        return function( scope, elem, attrs ) {
            elem.bind('blur', function() {
                scope.$apply(attrs.ngBlur);
            });
        };
    }).directive('contenteditable', function() {
        return {
            require: 'ngModel',
            link: function(scope, elm, attrs, ctrl) {
                // view -> model
                elm.bind('blur', function() {
                    scope.$apply(function() {
                        ctrl.$setViewValue(elm.html());
                    });
                });

                // model -> view
                ctrl.$render = function() {
                    elm.html(ctrl.$viewValue);
                };

                // load init value from DOM
                ctrl.$setViewValue(elm.html());
            }
        };
    })
    .directive('ngFocus', function( $timeout ) {
        return function( scope, elem, attrs ) {
            scope.$watch(attrs.ngFocus, function( newval ) {
                if ( newval ) {
                    $timeout(function() {
                        elem[0].focus();
                    }, 0, false);
                }
            });
        };
    })
    .directive('tabs',function () {
        return {
            restrict: 'E',
            transclude: true,
            scope: {},
            controller: function ($scope, $element) {
                var panes = $scope.panes = [];

                $scope.select = function (pane) {
                    angular.forEach(panes, function (pane) {
                        pane.selected = false;
                    });
                    pane.selected = true;
                }

                this.addPane = function (pane) {
                    if (panes.length == 0) $scope.select(pane);
                    panes.push(pane);
                }
            },
            template: '<div class="tabbable">' +
                '<ul class="nav nav-tabs">' +
                '<li ng-repeat="pane in panes" ng-class="{active:pane.selected}">' +
                '<a href="" ng-click="select(pane)">{{pane.title}}</a>' +
                '</li>' +
                '</ul>' +
                '<div class="tab-content" ng-transclude></div>' +
                '</div>',
            replace: true
        };
    }).directive('pane', function () {
        return {
            require: '^tabs',
            restrict: 'E',
            transclude: true,
            scope: { title: '@' },
            link: function (scope, element, attrs, tabsCtrl) {
                tabsCtrl.addPane(scope);
            },
            template: '<div class="tab-pane" ng-class="{active: selected}" ng-transclude>' +
                '</div>',
            replace: true
        };
    });
mod.directive('typeahead', function () {
        return {
            restrict: 'A',
            transclude: false,
            require: '^ngModel',
            scope: false,
            controller: function ($scope, $element, $attrs) {
                //$scope.id = $attrs.inputId + '" ng-model="$parent.' + $attrs.ngModel;
                var options = widgets.typeaheads[$attrs['typeahead']]({
                    min: $attrs['min-length'],
                    select: $scope[$attrs['typeaheadSelect']],
                    element: $($element)
                });
                var ta = $($element).typeahead(options);
            }
        };
    })
    .directive('decimalOnly', function () {
        return {
            restrict: 'A',
            transclude: false,
            require: '^ngModel',
            scope: false,
            controller: function ($scope, $element, $attrs) {

                var ta = $($element).on("keypress keyup blur", function (event) {
                    //this.value = this.value.replace(/[^0-9\.]/g,'');
                    $(this).val($(this).val().replace(/[^0-9\.]/g, ''));
                    if ((event.which != 46 || $(this).val().indexOf('.') != -1) && (event.which < 48 && event.which > 57)) {
                        event.preventDefault();
                    }
                });
            }
        };
    })
    .directive('maxLength', function () {
        return {
            restrict: 'A',
            transclude: false,
            require: '^ngModel',
            scope: false,
            controller: function ($scope, $element, $attrs) {

                var ta = $($element).on("keypress keyup blur", function (event) {

                    var ok = [37, 38, 39, 40]; // use this list to indicate key strokes that are ok to pass on to the browser

                    if ($(this).val().length === parseInt($attrs.maxLength) && !event.ctrlKey && ok.indexOf(event.which) === -1) { // allow keystrokes when ctrl is pressed
                        event.preventDefault();
                    }
                });
            }
        };
    })
angular.module('myApp.directives', []).directive('currency', function () {
    return {
        restrict: 'A',
        transclude: true,
        scope: false,
        require: '^ngModel',
        controller: function ($scope, $element, $attrs) {
            $scope.$watch($attrs.ngModel, function (newValue, oldValue) {
                var val = newValue.toString().replace(/[^\d.-]/g, '');
                val = val.length > 0 ? val : '""';
                while (val.split(".").length - 1 > 1)
                    val = val.replace('.', '');
                while (val.split("-").length - 1 > 1)
                    val = val.replace('-', '');
                switch (val.substr(val.length - 1)) {
                    case '.':
                        val = '"' + val + '"';
                        break;
                    case '-':
                        val = '"' + val + '"';
                        break;
                    case '0':
                        val = '"' + val + '"';
                        break;
                }
                eval('$scope.' + $attrs.ngModel + ' = ' + val + '');

            });
        }
    };
});


