'use strict';

/* Controllers */

var app = angular.module('myApp.controllers', []);

app.controller('site_controller', ['$scope', '$http', function ($scope, $http) {

    $scope.deauthenticate = function () {
        $http.post('/api/system/deauthenticate').success(function (data) {
            if (data.errors) {
                alert(errors.toString());
            } else {
                window.location = data.login_url;
            }
        });
    }
}])
;

app.controller('login_controller', ['$scope', '$http', '$routeParams', function ($scope, $http, $routeParams) {

    $http.get('/api/system/databases').success(function (data) {
        $scope.databases = data.databases;
        $scope.database = data.databases[0].id;
    });

    $scope.login = function () {
        $http.post('/api/system/authenticate', {username: $scope.username, password: $scope.password, database: $scope.database}).success(function (data) {
            if (data.errors && data.errors.length > 0) {
                $scope.errors = data.errors;
            } else {
                window.location = '/' + window.location.hash;
            }
        });
    }

}]);
app.controller('dashboard_controller', ['$scope', '$http', '$routeParams', '$keyboardManager', '$window', '$route', function ($scope, $http, $routeParams, $keyboardManager, $window, $route) {


}]);
