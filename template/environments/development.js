
module.exports = {
	port: 5000,
    encryption_key: '<Enter General Encryption Key>',
	shortname: 'VG',
	longname: 'Vanguard Site',
	databases: {
		"TESTDB": {
            encryption_key: 'TEST',
            name: "TESTDB",
            adapter: 'mstds',
            schema: "dbo",
            database: "TESTDB",
            options: {
                database: "TESTDB"
            },
            userName: 'sa',
            password: 'test',
            server: '127.0.0.1',
            map: {
                /*  Here you can define custom table mappings incase you want to alias your table names */

                /*
                'tickets': {
                    alias: 'SQL29I.dbo.TICKETS',
                    id_key: 'recnum'
                },
                'customer': {
                    alias: 'SQL29I.dbo.CUSTOMER',
                    id_key: 'recnum'
                }*/
            }
        }
	},
    smtp: {
        user: 'john@doe.com',
        password: 'smtpPass',
        host: 'secure.emailsrvr.com',
        ssl: true,
        from: 'Vanguard Sample <john@doe.com>'
    },
    sessions: {
        type: 'sqlite3',
        key: '<enter_session_key>'
    }
};