exports.initializeTask = function(taskOptions){
    if (!global.path) global.path = require('path');
    var vgApp = exports;

    var opts = require(require('path').resolve('./environments/' + process.argv[2]));

    opts.disableWebServer = true;

    vgApp.initialize(opts);

    process.env.TZ = '+0:00';
    process.env.tz = '+0:00';

    // Setup Basic Ruleset

    vg.config.rules = vg.config.rules ? vg.config.rules : {};
    vg.config.rules.password_minimum_length = vg.config.rules.password_minimum_length ? vg.config.rules.password_minimum_length : 2;
    vg.config.rules.password_maximum_length = vg.config.rules.password_maximum_length ? vg.config.rules.password_maximum_length : 8;

};

exports.initialize = function (options) {
    if (!global.path) global.path = require('path');
    // Initialize Vanguard Core Framework
    global.vg = require('./lib/core');
    global.scheduler = require('node-schedule');


    // Initialize Configuration
    vg.config = options ? options : require('./config');

    // Utility Initialization
    require('./lib/utilities');

    // Initialize Database list
    vg.db = vg.db ? vg.db : {};
    _.each(_.pairs(vg.config.databases), function (pair) {
        vg.db[pair[0]] = vg.db[pair[0]] ? vg.db[pair[0]] : _.clone(pair[1]);
    });

    // Initialize Models
    if (fs.existsSync('./app/models')) {
        var files = fs.readdirSync('./app/models');

        _.each(_.pairs(vg.config.databases), function (pair) {
            var dbname = pair[0];
            var database = pair[1].database;
            var schema = pair[1].schema;

            // Ensure proper structure in the db and db[name].models
            vg.db = vg.db ? vg.db : {};
            vg.db[dbname] = vg.db[dbname] ? vg.db[dbname] : _.clone(pair[1]);
            vg.db[dbname].models = vg.db[dbname].models ? vg.db[dbname].models : {};

            files.forEach(function (file, index) {
                if (file.toLowerCase().indexOf('_model') === -1) return;
                var m = require(path.resolve('./app/models' + "/" + file))(database, schema);
                vg.db[dbname].models[m.name] = m;
            });

        });

    }
    // Initialize Workers
    if (fs.existsSync('./app/workers')) {
        var files = fs.readdirSync('./app/workers');

        _.each(_.pairs(vg.config.databases), function (pair) {
            var dbname = pair[0];
            var database = pair[1].database;
            var schema = pair[1].schema;

            // Ensure proper structure in the db and db[name].workers
            vg.db = vg.db ? vg.db : {};
            vg.db[dbname] = vg.db[dbname] ? vg.db[dbname] : _.clone(pair[1]);
            vg.db[dbname].workers = vg.db[dbname].workers ? vg.db[dbname].workers : {};

            files.forEach(function (file, index) {
                if (file.toLowerCase().indexOf('_worker') === -1) return;
                var m = require(path.resolve('./app/workers' + "/" + file))(database, schema);
                vg.db[dbname].workers[m.name] = m;
            });

        });

    }

    // Initialize Scheduled jobs ============================================================
    if (fs.existsSync('./app/schedules')) {
        files = fs.readdirSync('./app/schedules');
        _.each(_.pairs(vg.config.databases), function(pair) {
            var dbname = pair[0];
            var database = pair[1].database;
            var schema = pair[1].schema;

            // Ensure proper structure in the db and db[name].scheduledJob
            vg.db = vg.db ? vg.db : {};
            vg.db[dbname] = vg.db[dbname] ? vg.db[dbname] : _.clone(pair[1]);
            vg.db[dbname].scheduledJobs = vg.db[dbname].scheduledJobs ? vg.db[dbname].scheduledJobs : {};

            files.forEach(function (file, index) {
                if (file.toLowerCase().indexOf('_schedule') === -1) return;
                var m = require(path.resolve('./app/schedules' + "/" + file))(database, schema);
                // only install if current database is found in scheduled list.
                if (m.databases.indexOf(dbname) > -1) {
                    if (vg.db[dbname].scheduledJobs[m.name]) {
                        console.log('WARNING: Scheduled job "' + m.name + '" already exists and will NOT be overwritten by ' + file);
                    } else {
                        vg.db[dbname].scheduledJobs[m.name] = m;
                        m.install();
                    }
                }
            });
        });
    }

    if (!options || options && !options.disableWebServer) {
        // Initialize Express Application
        var express = require('express');
        vg.app = express();

        vg.app.use(express.cookieParser());

        // Build Session Store
        var store = null;
        switch (vg.config.sessions && vg.config.sessions.type ? vg.config.sessions.type.toLowerCase() : 'memory') {
            case 'memory':
                store = new express.session.MemoryStore();
                break;
            case 'filesystem':
                var FSStore = require('./lib/connect-fs')(express);

                if (!fs.existsSync(path.resolve('./sessions')))
                    fs.mkdirSync(path.resolve('./sessions'));

                store = new FSStore({dir: path.resolve('./sessions')});
                break;
            case 'mongo':
                var MongoStore = require('connect-mongo')(express);

                store = new MongoStore(vg.config.sessions);

                break;
            case 'sqlite3':
                var SQLiteStore = require('connect-sqlite3')(express);
                store = new SQLiteStore;
                break;
        }

        vg.app.use(express.session({secret: vg.config.sessions.key, store: store}));

        vg.app.use(express.bodyParser({ keepExtensions: true, uploadDir: '/uploads', limit: vg.config.request_limit ? vg.config.request_limit : '50mb' }));
        // Initialize Routing
        require(path.resolve('./app/routes'));
        vg.app.use(express.static('./public'));

        // Start Listening
        console.log('Starting Vanguard Server ...');
        vg.app.listen(vg.config.port);
        console.log('Listening on port ' + vg.config.port);
    }
}