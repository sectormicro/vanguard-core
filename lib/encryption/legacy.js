module.exports = {encrypt: function (key, value) {
    if (!key) key = '399';
    var offset = vg.utils.crypto.legacy.calculateOffset(key) % 94;
    var encrypted = '';
    var result = '';
    if (value) {
        result = '\\a';
        _.each(value.split(''), function (chr) {
            var ascii = chr.charCodeAt(0);
            ascii += offset - 33;
            if (ascii < 0) ascii *= -1;
            ascii = (ascii % 94) + 33;
            if (String.fromCharCode(ascii) === '\\') result += ' ';
            else result += String.fromCharCode(ascii);
        });
        result += '\\';

    }
    return result;
},
is_encrypted: function (value) {
    return value && value.length > 2 && value.substr(0, 2) === '\\a' && value.substr(value.length - 1) === '\\';
},
calculateOffset: function (key) {
    var val = 0;
    _.each(key.split(''), function (chr) {
        val += chr.charCodeAt();
    });
    return val;
}
};
