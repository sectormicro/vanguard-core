/**
 * Vanguard RC4 Encryption Algorithm
 *     Note this function converts from String to Integer (char codes) and then directly to Base64
 * This avoids any possible issues that could arise due to Encoding.
 *
 * Sector Micro Computer, Inc.
 * MIT License
 * Contributors:
 *     Andrew Kanieski (ASK)
 *
 * History:
 *     ASK 2014-01-20: Initial Port from Dataflex.
 *					   Uses base64ArrayBuffer to bypass encoding (public domain)
 */
module.exports = (function(){
    var RC4SBXB = [];
    var RC4SBXA = [];

    var reverseBase64Binary = {
        _keyStr : "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=",

        /* will return a  Uint8Array type */
        decodeArrayBuffer: function(input) {
            var bytes = (input.length/4) * 3;
            var ab = new ArrayBuffer(bytes);
            this.decode(input, ab);

            return ab;
        },

        decode: function(input, arrayBuffer) {
            //get last chars to see if are valid
            var lkey1 = this._keyStr.indexOf(input.charAt(input.length-1));
            var lkey2 = this._keyStr.indexOf(input.charAt(input.length-2));

            var bytes = (input.length/4) * 3;
            if (lkey1 == 64) bytes--; //padding chars, so skip
            if (lkey2 == 64) bytes--; //padding chars, so skip

            var uarray;
            var chr1, chr2, chr3;
            var enc1, enc2, enc3, enc4;
            var i = 0;
            var j = 0;

            if (arrayBuffer)
                uarray = new Uint8Array(arrayBuffer);
            else
                uarray = new Uint8Array(bytes);

            input = input.replace(/[^A-Za-z0-9\+\/\=]/g, "");

            for (i=0; i<bytes; i+=3) {
                //get the 3 octects in 4 ascii chars
                enc1 = this._keyStr.indexOf(input.charAt(j++));
                enc2 = this._keyStr.indexOf(input.charAt(j++));
                enc3 = this._keyStr.indexOf(input.charAt(j++));
                enc4 = this._keyStr.indexOf(input.charAt(j++));

                chr1 = (enc1 << 2) | (enc2 >> 4);
                chr2 = ((enc2 & 15) << 4) | (enc3 >> 2);
                chr3 = ((enc3 & 3) << 6) | enc4;

                uarray[i] = chr1;
                if (enc3 != 64) uarray[i+1] = chr2;
                if (enc4 != 64) uarray[i+2] = chr3;
            }

            return uarray;
        }
    };

    // Converts an ArrayBuffer directly to base64, without any intermediate 'convert to string then
    // use window.btoa' step. According to my tests, this appears to be a faster approach:
    // http://jsperf.com/encoding-xhr-image-data/5
    // Source: https://gist.github.com/akanieski/8528684
    function base64ArrayBuffer(arrayBuffer) {
        var base64 = ''
        var encodings = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/'

        var bytes = new Uint8Array(arrayBuffer)
        var byteLength = bytes.byteLength
        var byteRemainder = byteLength % 3
        var mainLength = byteLength - byteRemainder

        var a, b, c, d
        var chunk

        // Main loop deals with bytes in chunks of 3
        for (var i = 0; i < mainLength; i = i + 3) {
            // Combine the three bytes into a single integer
            chunk = (bytes[i] << 16) | (bytes[i + 1] << 8) | bytes[i + 2]

            // Use bitmasks to extract 6-bit segments from the triplet
            a = (chunk & 16515072) >> 18 // 16515072 = (2^6 - 1) << 18
            b = (chunk & 258048) >> 12 // 258048   = (2^6 - 1) << 12
            c = (chunk & 4032) >> 6 // 4032     = (2^6 - 1) << 6
            d = chunk & 63 // 63       = 2^6 - 1

            // Convert the raw binary segments to the appropriate ASCII encoding
            base64 += encodings[a] + encodings[b] + encodings[c] + encodings[d]
        }

        // Deal with the remaining bytes and padding
        if (byteRemainder == 1) {
            chunk = bytes[mainLength]

            a = (chunk & 252) >> 2 // 252 = (2^6 - 1) << 2

            // Set the 4 least significant bits to zero
            b = (chunk & 3) << 4 // 3   = 2^2 - 1

            base64 += encodings[a] + encodings[b] + '=='
        } else if (byteRemainder == 2) {
            chunk = (bytes[mainLength] << 8) | bytes[mainLength + 1]

            a = (chunk & 64512) >> 10 // 64512 = (2^6 - 1) << 10
            b = (chunk & 1008) >> 4 // 1008  = (2^6 - 1) << 4

            // Set the 2 least significant bits to zero
            c = (chunk & 15) << 2 // 15    = 2^4 - 1

            base64 += encodings[a] + encodings[b] + encodings[c] + '='
        }

        return base64
    }

    function process(Key, raw_text) {
        var Text = [];
        for (var temp = 0; temp < raw_text.length; temp++) {
            Text.push(_.isString(raw_text) ? raw_text.charCodeAt(temp) : raw_text[temp]);
        }
        var ix, iy, offset, origlen;
        var ixtmp, iytmp, ixytmp, ixc;
        var sRet = []; //new Int32[Text.Length];
        for (var temp = 0; temp < Text.length; temp++)
            sRet.push(null);
        for (var temp = 0; temp < 256; temp++) {
            RC4SBXB.push(null);
            RC4SBXA.push(null);
        }
        Create_RC4_key(Key);
        origlen = Text.length;

        iy = 0;

        for (offset = 1; offset <= origlen; offset++) {
            ix = ((offset - 1) % 256);
            ixtmp = RC4_Array_Value(ix);
            iy = ((iy + ixtmp) % 256);

            ixtmp = RC4_Array_Value(ix);
            iytmp = RC4_Array_Value(iy);

            Set_RC4_Array_Value(ix, iytmp);
            Set_RC4_Array_Value(iy, ixtmp);

            ixc = Text[offset - 1];
            ixtmp = RC4_Array_Value(ix);
            iytmp = RC4_Array_Value(iy);
            ixytmp = RC4_Array_Value(((ixtmp + iytmp) % 256));
            ixc = ixc ^ ixytmp; // Logical XOR
            sRet[offset - 1] = ixc;
        }
        return sRet;
    }

    function Set_RC4_Array_Value(iIndex, iVal) {
        if (iIndex < 128) RC4SBXA[iIndex] = iVal;
        else RC4SBXB[iIndex - 128] = iVal;
    }

    function RC4_Array_Value(iIndex) {
        var iRet;
        if (iIndex < 128) iRet = RC4SBXA[iIndex];
        else iRet = RC4SBXB[iIndex - 128];
        return iRet;
    }


    function Create_RC4_key(Key) {
        var a, b, keylen, idx, ikey, atmp, btmp;

        keylen = Key.length;
        b = 0;

        for (var idx = 0; idx < 256; idx++)
            Set_RC4_Array_Value(idx, idx);

        // encode key array
        for (a = 0; a < 256; a++) {
            ikey = (Key.substr(a % keylen, 1)).charCodeAt(0);
            atmp = RC4_Array_Value(a);
            b = ((b + atmp + ikey) % 256);

            btmp = RC4_Array_Value(b);

            Set_RC4_Array_Value(a, btmp);
            Set_RC4_Array_Value(b, atmp);
        }
    }
    this.encrypt = function(key, value) {
        return base64ArrayBuffer(process(key, value));
    }
    this.decrypt = function(key, value) {
        var reverseBase64Binary = {
            _keyStr : "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=",

            /* will return a  Uint8Array type */
            decodeArrayBuffer: function(input) {
                var bytes = (input.length/4) * 3;
                var ab = new ArrayBuffer(bytes);
                this.decode(input, ab);

                return ab;
            },

            decode: function(input, arrayBuffer) {
                //get last chars to see if are valid
                var lkey1 = this._keyStr.indexOf(input.charAt(input.length-1));
                var lkey2 = this._keyStr.indexOf(input.charAt(input.length-2));

                var bytes = (input.length/4) * 3;
                if (lkey1 == 64) bytes--; //padding chars, so skip
                if (lkey2 == 64) bytes--; //padding chars, so skip

                var uarray;
                var chr1, chr2, chr3;
                var enc1, enc2, enc3, enc4;
                var i = 0;
                var j = 0;

                if (arrayBuffer)
                    uarray = new Uint8Array(arrayBuffer);
                else
                    uarray = new Uint8Array(bytes);

                input = input.replace(/[^A-Za-z0-9\+\/\=]/g, "");

                for (i=0; i<bytes; i+=3) {
                    //get the 3 octects in 4 ascii chars
                    enc1 = this._keyStr.indexOf(input.charAt(j++));
                    enc2 = this._keyStr.indexOf(input.charAt(j++));
                    enc3 = this._keyStr.indexOf(input.charAt(j++));
                    enc4 = this._keyStr.indexOf(input.charAt(j++));

                    chr1 = (enc1 << 2) | (enc2 >> 4);
                    chr2 = ((enc2 & 15) << 4) | (enc3 >> 2);
                    chr3 = ((enc3 & 3) << 6) | enc4;

                    uarray[i] = chr1;
                    if (enc3 != 64) uarray[i+1] = chr2;
                    if (enc4 != 64) uarray[i+2] = chr3;
                }

                return uarray;
            }
        };
        var r = process(key, reverseBase64Binary.decode(value)), result = '';
        for (x = 0; x < r.length; x++) {
            result = result + String.fromCharCode(r[x]);
        }
        return result;
    }
    return this;
})();