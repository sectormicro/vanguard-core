/* Vanguard Javascript Web Development Framework
 * ---------------------------------------------
 * This file controls all routing for the website.
 *
 * History:
 * --------
 * 2013-09-20 ASK: Initial Development
 */

vg.app.routing = {

    controllers: {},

    _before_all: function(req, res, next){
        if (vg.app.routing.before_all){
            vg.app.routing.before_all(req,res,next);
        } else {
            next();
        }
    },

    check_authentication: function (p, authentication_strategy) {
        var authStrategy = authentication_strategy;
        var path = p;
        var check_auth = typeof authStrategy === 'undefined' ? false : _.isObject(authStrategy) ? authStrategy.check_for_user : authStrategy;
        var controllers = this.controllers;
        return (function(strategy){ return function (req, res) {
            var authorized = false;
            // Check Authentication
            if (check_auth) {
                authorized = req.session && req.session[authStrategy.user_property ? authStrategy.user_property : 'user'] ? true : false;
            } else {
                authorized = true;
            }

            if (authorized) {
                if (strategy && _.isFunction(strategy.success)) {
                    strategy.success(req, res, strategy, controllers[path.split('.')[0]][path.split('.')[1]]);
                } else {
                    controllers[path.split('.')[0]][path.split('.')[1]](req, res);
                }
            } else
                res.send(401);
        };})(authStrategy);
    },

    setRoute: function (verb, url, path, check_auth) {
        var router = this;


        if (_.isFunction(path)) { // Routed to custom function

            vg.app[verb](url, path);

        } else { // Controller Routing

            // Cache Controllers
            if (!router.controllers[path.split('.')[0]])
                router.controllers[path.split('.')[0]] = require('../../../app/controllers/' + path.split('.')[0] + '_controller');

            // Ensure before routes are executed before any others
            if (router.controllers[path.split('.')[0]].before) {
                vg.app[verb](url, router.controllers[path.split('.')[0]].before);
            }

            // Apply standard routes
            vg.app[verb](url, router.check_authentication(path, check_auth));
        }

    },

    get: function (url, path, check_auth) {
        this.setRoute('get', url, path, check_auth);
    },

    post: function (url, path, check_auth) {
        this.setRoute('post', url, path, check_auth);
    },

    put: function (url, path, check_auth) {
        this.setRoute('put', url, path, check_auth);
    },

    delete: function (url, path, check_auth) {
        this.setRoute('delete', url, path, check_auth);
    },

};

// Apply General Routes

vg.app.all('*', vg.app.routing._before_all);