var smc = require('./core');

var Model = function (name, settings) {
    var self = this;
    self.schema = settings.schema ? settings.schema : 'dbo';
    self.table_name = settings.table_name.toLowerCase();
    self.id_key = settings.id_key.toLowerCase();
    self.database = settings.database;
    self.name = name;
    self.properties = settings.properties;
    self.instanceFunctions = settings.instanceFunctions;
    self.classFunctions = settings.classFunctions;

    self.initialize = function(){

        self._attachClassFunctions();

        // Pickup definition settings possible found in the database mapping configuration
        if (!vg.db[self.database].map)
            vg.db[self.database].map = {};
        if (!vg.db[self.database].map[self.table_name])
            vg.db[self.database].map[self.table_name] = {};
        if (vg.db[self.database].map[self.table_name].id_key)
            self.id_key = vg.db[self.database].map[self.table_name].id_key;
        else
            vg.db[self.database].map[self.table_name].id_key = self.id_key;
        if (vg.db[self.database].map[self.table_name].alias)
            self.table = vg.db[self.database].map[self.table_name].alias;
        else {
            vg.db[self.database].map[self.table_name].alias = self.table_name;
            self.table = ('[' + self.table_name + ']');
        }

        // Update the map in memory to include settings that may be missing.

        var temp_map = vg.db[self.database].map[self.table_name];
        if (!temp_map.properties) temp_map.properties = {};
        _.each(_.pairs(self.properties), function(pair){
            var name = pair[0];
            var options = pair[1];
            if (!temp_map.properties[name])
                temp_map.properties[name] = {};
            if (!temp_map.properties[name].formatted)
                temp_map.properties[name].formatted = self.formatColumn(options.column_name ? options.column_name : name);
        });
    }

    // attach provided class functions
    self._attachClassFunctions = function () {
        vg._.each(vg._.functions(self.classFunctions), function (f) {
            self[f] = self.classFunctions[f];
        });
    }

    // @updateAttributes
    // Updates model attributes with provided attributes
    self._updateAttributes = function (attr) {
        var item = this;
        _.each(_.pairs(attr), function (pair) {

            item[pair[0]] = pair[1];

        });
    }

    self._validateProperties = function() {
        var item = this;
        var valid = true;
        _.each(_.pairs(item.model.properties), function(pair){
            var name = pair[0];
            var options = pair[1];
            var title = options.title ? options.title : name;
            if (options.max_length && item[name] && item[name].toString().length > options.max_length) {
                item.errors.push(title + ' exceeds the maximum length');
                valid = false;
            }
            if (options.min_length && item[name] && item[name].toString().length < options.min_length) {
                item.errors.push(title + ' is not long enough');
                valid = false;
            }
        });
        return valid;
    }

    // attach provided class functions
    self._attachInstanceFunctions = function (i) {
        // attach instance functions
        vg._.each(vg._.functions(self.instanceFunctions), function (f) {
            i[f] = self.instanceFunctions[f];
        });
        i.save = self._save;
        i.updateAttributes = self._updateAttributes;
        i.toJSON = self._toJSON;
        i._validateProperties = self._validateProperties;
    }

    self._toJSON = function() {
        var t = _.clone(this);
        //delete t.toJSON;
        delete t._raw;
        delete t.model;
        delete t._original;
        delete t.save;
        delete t.updateAttributes;
        return t;
    };

    // adds the provided instance function to all instances thereafter
    self.addInstanceFunction = function (name, f) {
        self.instanceFunctions[name] = f;
    };

    // adds the provided class function to the current Model
    self.addClassFunction = function (name, f) {
        self.classFunctions[name] = f;
        self[name] = f;
    };

    // used to mold untyped objects into data modeled objects
    self.mold = function (results) {
        return vg.db.getAdapter(self.database).mold(self, results);
    };

    // Use default query settings provided otherwise use default, default settings
    self.defaultQuerySettings = settings.defaultQuerySettings ? settings.defaultQuerySettings : {
        // Nothing defaulted
    };

    self.open = function(callback) {
        vg.db.getAdapter(self.database).openConnection(vg.config.databases[self.database], callback);
    }

    //  @query(sql,settings,callback)
    //	general querying method
    //
    self.query = function (sql, qSettings, callback) {
        vg.db.getAdapter(self.database).query(this, sql, qSettings, callback);
    };

    //  @query_procedure(sql,settings,callback)
    //	general querying method for stored procedures
    //
    self.query_procedure = function (sql, qSettings, callback) {
        vg.db.getAdapter(self.database).query_procedure(this, sql, qSettings, callback);
    };


    self.formatter = function (model) {
        return vg.db.getAdapter(self.database).formatter(model);
    };

    self.formatColumn= function (name) {
        return vg.db.getAdapter(self.database).formatColumn(name);
    };

    self.formatTable = function (name) {
        return vg.db.getAdapter(self.database).formatTable(self.database, name);
    };

    self.formatValue = function (value) {
        return vg.db.getAdapter(self.database).formatValue(value);
    };

    // @create(attributes)
    // Create a new model instance
    self.create = function (attributes) {
        var item = attributes || {};
        item.model = self;
        self._attachInstanceFunctions(item);
        if (self.afterCreated) item = self.afterCreated(item);

        return item;
    }

    // @save(options,callback)
    // Instance Function: Saves the current item. It will insert if not already inserted and update if it does already exist
    self._save = function (options, callback) {
        vg.db.getAdapter(self.database).save(self, this, options, callback);
    }

    // @drop(options)
    // Drops the model's table from the database [Warning - Data loss]
    self.drop = function (options) {
        vg.db.getAdapter(self.database).drop(this, options);
    }

    // @sync(options)
    // Synchronize model with database ensuring proper schema. Will not drop table if already exists
    self.sync = function (options) {
        vg.db.getAdapter(self.database).sync(this, options);
    };

    // Finally
    self.initialize();
    return self;

}

exports.define = function (name, settings) {
    return new Model(name, settings);
};