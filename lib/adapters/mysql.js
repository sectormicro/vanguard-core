var fs = require('fs');

exports.sql = require('mysql');

/*
 * Establishes connection with a MySQL database. The configuration object passed in the info parameter should look similar to the
 * following:
 *
 * {
 *     host: 'localhost',
 *     user: 'root',
 *     password: 'mypassword',
 *     database: 'username',
 *     multipleStatements: true
 * }
 *
 * Please see https://npmjs.org/package/mysql for more details on the options you can set for a MySQL connection.
 *
 * @param {object} info object holding configuration settings to apply to a MySQL database connection.
 * @param {object} callback function to invoke after successul/failed attempt at establishing DB connection. Check err parameter in callback for any errors.
 */
exports.openConnection = function (info, callback) {
    // For mySQL due to security reasons, multiple statement queries is disabled by default (see https://npmjs.org/package/mysql for details).
    // also mysql uses host, not server and user, not userName
    info.multipleStatements = true;
    info.host = info.server;
    info.user = info.userName;
    var connection = exports.sql.createConnection(info);

    connection.connect(function (err) {
        if (err) {
            console.log('Failed to establish connection.');
        } else {
            //console.log('Connection established.');
            connection.queryRaw = exports.queryRaw;
            connection.queryRawBatch = exports.queryRawBatch;
        }
        callback(err, connection);
    });
};

/*
 * Executes a SQL statement against MySQL w/o any model formatting on the results performed by vanguard-core.
 *
 * @param {string} sql DML statement to execute against MySQL.
 * @param {object} callback function to invoke after sql DML execution has completed. Callback will contain two parameters: err and data. Check err for any errors. Rows will contain one or more records representing the result set returned from MySQL.
 */
exports.queryRaw = function (sql, callback) {
    //console.log('Raw Query: ' + sql);

    this.query(sql, function (err, rows, fields) {
        if (err) console.log(err);
        var data = [];

        // it's possible that rows is a single object, not an array.
        if (Object.prototype.toString.call(rows) === '[object Array]') {
            for (var i = 0; i < rows.length; i++) {
                var entry = {};
                for (var f in rows[i]) {
                    entry[f.toLowerCase()] = rows[i][f];
                    if (_.isDate(entry[f.toLowerCase()])) {
                        entry[f.toLowerCase()] = entry[f.toLowerCase()].toLocal();
                    }
                }
                data.push(entry);
            }
        } else {
            var singleEntry = {};
            for (var x in rows) {
                singleEntry[x.toLowerCase()] = rows[x];
                if (_.isDate(singleEntry[x.toLowerCase()])) {
                    singleEntry[x.toLowerCase()] = singleEntry[x.toLowerCase()].toLocal();
                }
            }
            data.push(singleEntry);
        }
        callback(err, data);
    });
};

/*
 * Executes a series of SQL statements against MySQL, result sets returned are molded into models by vanguard-core.
 *
 * @param {string} sql statement to execute against MySQL.
 * @param {object} settings configuration object holding settings for vanguard-core (i.e. connection information, models, etc...)
 * @callback {object} function to invoke after batch query execution is complete. Will contain two arguments: err and payload. Check err for any errors that may have occured, payload contains 1 or more models representing the results returned by MySQL.
 */
exports.queryBatch = function (sql, settings, callback) {
    var payload = [];
    var adapter = this;
    flow.exec(function () {
            if (!settings.connection) {
                adapter.openConnection(adapter.connection_info, this);
            } else {
                this(undefined, settings.connection);
            }
        },
        function (err, connection) {
            connection.queryRawBatch(sql, function (err, set) {
                if (!set || err) {
                    callback(err, payload);
                } else if (settings.models && set.length != settings.models.length)
                    callback('Query returned with more sets than expected');
                else {
                    if (settings.sets) {
                        payload = {};
                        _.each(settings.sets, function (set_item, i) {
                            payload[set_item.name] = exports.mold(set_item.model, set[i]);
                        });
                    } else {
                        _.each(settings.models, function (model, i) {
                            payload.push(exports.mold(model, set[i]));
                        });
                    }
                    callback(err, payload);
                }
            });
        }
    );
};


/*
 * Updates one or more MySQL database tables with new or existing model changes.
 *
 * @param {object} collection List of objects holding one or more models representing new records to be added or existing records to be updated.
 * @param {object} settings Configuration object holding settings for vanguard-core (i.e. connection information, models, etc...)
 * @callback {object} callback Function to invoke after batch updates are complete. Will contain two arguments: err and collection. Check err for any errors that may have occured, collection contains 1 or more models that were used during the update process. New records added will contain an ID key value.
 */
exports.saveCollection = function (collection, settings, callback) {
    var sql = '';

    _.each(collection, function (item) {
        sql += exports.saveSql(settings.model, item, {});
    });

    settings.connection.queryRawBatch(sql, function (err, set) {
        _.each(collection, function (item, i) {
            if (!(item[settings.model.id_key] && item[settings.model.id_key] != 0)) {
                try {
                    item[settings.model.id_key] = set[i][0][settings.model.id_key];
                } catch (e) {
                    console.log(e);
                }
            }
        });

        callback(err, collection);
    });
};

/*
 * Executes a series of SQL statements against MySQL w/o any model formatting on the results performed by vanguard-core.
 * @param {string} sql multiple sql DML statements to execute against MySQL.
 * @param {object} callback function to invoke after sql DML execution has completed. Callback will contain two parameters: err and set. Check err for any errors. set will contain records from one or more tables returned from MySQL.
 */
exports.queryRawBatch = function (sql, callback) {
    var conn = this;
    //console.log('Raw Query: ' + sql);
    this.query(sql, function (err, results) {
        var set = [];
        var offset = -1;

        // iterate through the results from multiple tables
        _.each(results, function (rows) {
            var d = [];

            _.each(rows, function (row, i) {
                if (i >= offset) {
                    var entry = {};
                    _.each(row, function (field) {
                        entry[field.metadata.colName.toLowerCase()] = field.value;
                    });
                    d.push(entry);
                }
            });

            offset = rows.length;
            set.push(d);
        });

        if (err) callback(err);
        callback(undefined, set);
    });
};

/*
 * Opens and starts a transaction.
 *
 * @param {object} info Object holding a series of configuration settings to apply to an MySQL connection.
 * @param {object} callback Callback function to invoke after connection has been opened and transaction started, or if an error occurs. Check err for any errors that may have occurred, otherwise connection object parameter in callback represents the open and started transaction.
 */
exports.open_begin = function (info, callback) {
    info.rowCollectionOnRequestCompletion = true;
    // For mySQL due to security reasons, multiple statement queries is disabled by default (see https://npmjs.org/package/mysql for details).
    info.multipleStatements = true;
    var connection = exports.sql.createConnection(info);
    info.host = info.server;
    info.user = info.userName;
    connection.connect(function (err) {
        if (err) {
            console.log('Failed to establish connection.');
            callback(err);
        } else {
            //console.log('Connection established.');
            connection.queryRaw = exports.queryRaw;
            connection.queryRawBatch = exports.queryRawBatch;
            connection.beginTransaction(function (err) {
                callback(err, connection);
            });
        }
    });
};

/*
 * Traverses through a model's list of properties and returns a subset of properties whose cached option is set to true.
 *
 * @param {object} model Model to scan for cached properties.
 */
exports.select_cache = function (model) {
    var cache_columns = [];
    _.each(_.pairs(model.properties), function (pair) {
        var options = pair[1];
        var column_name = options.column_name ? options.column_name : pair[0];
        if (options.cached) {
            cache_columns.push(exports.formatColumn(column_name));
        }
    });
    if (cache_columns.length === 0 || cache_columns.indexOf(model.id_key) === -1)
        cache_columns.push(exports.formatColumn(model.id_key));
    return cache_columns.toString();
};

getColumnType = function (rawColVal) {
    var x = rawColVal.indexOf('(');
    return rawColVal.substr(0, x);
};

getColumnPrecisionAndScale = function (rawColVal) {
    var x = rawColVal.indexOf('(');
    var type = rawColVal.substr(0, x);
    var precisionScale = rawColVal.replace('(', '').replace(')', '').replace(type, '');
    return precisionScale.split(',');
};

/*
 * Uses MySQL database schema to generate a javascript controller for MVC development.
 *
 * @param {string} model_name Name of model to generate controller for.
 * @param {string} db Name of MySQL database instance to run schema queries against.
 * @param {string} table_name Name of MySQL table to pull up schema and generate controller off of.
 * @param {object} callback Callback function to invoke after successful/failed model generation. Check err for any errors that may have occurred, otherwise 2nd parameter will contain the controller that was successfully generated.
 */
exports.buildController = function (model_name, db, table_name, callback) {
    exports.openConnection(vg.config.databases[db], function (err, conn) {
        var sql = 'SHOW COLUMNS from ' + table_name;
        //console.log(sql);
        conn.queryRaw(sql, function (err, results) {
            try {
                if (err) throw new Error(err);
                var properties = [];
                var model = {model_name: model_name};
                _.each(results, function (row) {
                    var column_name = row.field.toLowerCase().replace('#', '_number');
                    var type = getColumnType(row.type);
                    var precisionScale = getColumnPrecisionAndScale(row.type);
                    var precision = precisionScale[0]
                    var scale = precisionScale > 1 ? precisionScale[1] : 0;
                    var nullable = row.null.toString().toLowerCase() === 'yes' ? true : false;
                    var maxlength = -1; // not supported in MySQL
                    var identity = row.key !== undefined ? true : false;

                    if (identity)
                        model.id_key = column_name;

                    var property = {
                        property_name: column_name,
                        column_name: column_name,
                        max_length: maxlength,
                        type: identity ? 'key' : type,
                        not_null: nullable ? false : false
                    };

                    switch (type.toLowerCase()) {
                        case 'varchar':
                            property.type = 'string';
                            break;
                    }
                    properties.push(property);
                });

                model.properties = properties;
                model.table_name = table_name;
                model.database = vg.config.databases[db].database;
                model.schema = vg.config.databases[db].schema;
                model.model_name_lower = model_name.toLowerCase();
                model.today = new Date().toString();

                var content = Mustache.render(fs.readFileSync(require('path').resolve(__dirname + '/../tmpl/controller.js.mustache'), 'utf-8'), model);
                fs.writeFileSync(require('path').resolve('./' + model_name.toLowerCase() + '_controller.js'), content);
                callback(undefined, model);

            } catch (err) {
                callback(err, false);
            }
        });
    });
};

/*
 * Uses MySQL database schema to generate a javascript model for MVC development.
 *
 * @param {string} model_name Name of new model to generate.
 * @param {string} db Name of MySQL database instance to run schema queries against.
 * @param {string} table_name Name of MySQL table to pull up schema and generate model off of.
 * @param {object} callback Callback function to invoke after successful/failed model generation. Check err for any errors that may have occurred, otherwise 2nd parameter will contain the model that was successfully generated.
 */
exports.buildModel = function (model_name, db, table_name, callback) {
    exports.openConnection(vg.config.databases[db], function (err, conn) {
        var sql = 'SHOW COLUMNS from ' + table_name;
        //console.log(sql);
        conn.queryRaw(sql, function (err, results) {
            try {
                if (err) throw new Error(err);
                var properties = [];
                var model = {model_name: model_name};
                _.each(results, function (row) {
                    var column_name = row.field.toLowerCase().replace('#', '_number');
                    var type = getColumnType(row.type);
                    var precisionScale = getColumnPrecisionAndScale(row.type);
                    var precision = precisionScale[0]
                    var scale = precisionScale > 1 ? precisionScale[1] : 0;
                    var nullable = row.null.toString().toLowerCase() === 'yes' ? true : false;
                    var maxlength = -1; // not supported in MySQL
                    var identity = row.key !== undefined ? true : false;

                    if (identity)
                        model.id_key = column_name;

                    var property = {
                        property_name: column_name,
                        column_name: column_name,
                        max_length: maxlength,
                        type: identity ? 'key' : type,
                        not_null: nullable ? false : false
                    };

                    switch (type.toLowerCase()) {
                        case 'varchar':
                            property.type = 'string';
                            break;
                    }
                    properties.push(property);
                });

                model.properties = properties;
                model.table_name = table_name;
                model.database = vg.config.databases[db].database;
                model.schema = vg.config.databases[db].schema;
                model.model_name_lower = model_name.toLowerCase();
                model.today = new Date().toString();

                var content = Mustache.render(fs.readFileSync(require('path').resolve(__dirname + '/../tmpl/model.js.mustache'), 'utf-8'), model);
                fs.writeFileSync(require('path').resolve('./' + model_name.toLowerCase() + '_model.js'), content);
                callback(undefined, model);

            } catch (err) {
                callback(err, false);
            }
        });
    });
};

/*
 * Converts results returned from MySQL into models for usage in MVC development.
 *
 * @param {object} model blank/template model to populate with results returned from MySQL database.
 * @results {object} results Array of one or more objects representing records returned from the MySQL database
 * @return {array} One or more models created from MySQL records.
 */
exports.mold = function (model, results) {
    var items = [];
    if (model !== 'raw') {
        _.each(results, function (row) {
            var qrow = {
                _raw: row,
                raw: row
            };

            // Iterate through each property
            _.each(_.pairs(model.properties), function (pair) {
                var propertyColumn = pair[1].column_name ? pair[1].column_name : pair[0];
                qrow[pair[0]] = row[propertyColumn];

                if (pair[1].type.toLowerCase() === 'json') {
                    try {
                        qrow[pair[0]] = JSON.parse(row[propertyColumn]);
                    } catch(err) {
                        //console.log('BAD JSON for field ' + propertyColumn + ' in ' + model.table_name + ' with id of ' + row[model.id_key]);
                    }
                }
                if (pair[1].type.toLowerCase() === 'csv') {
                    try {
                        qrow[pair[0]] = row[propertyColumn].indexOf(',') > -1  ? row[propertyColumn].split(',') : row[propertyColumn];
                    } catch(err) {
                        //console.log('BAD CSV for field ' + propertyColumn + ' in ' + model.table_name + ' with id of ' + row[model.id_key]);
                    }
                }

                if (pair[1].type.toLowerCase() === 'boolean' && row[propertyColumn]) {
                    qrow[pair[0]] = row[propertyColumn][0] === 1 ? true : false;
                }

            });

            model._attachInstanceFunctions(qrow);
            qrow.model = model;
            qrow._original = _.clone(qrow);
            items.push(qrow);

        });
    } else {
        items = results;
    }
    return items;
};

/*
 * Translates model and contents into either a SQL INSERT or UPDATE DML statement to be used against MySQL
 *
 * @param {object} model MVC model to translate from. model.id_key will determine if we should generate an INSERT statement (id_key = 0) or UPDATE.
 * @param {object} item represents a new or modified record to pass back to MySQL database.
 * @param {object} unused parameter. Contains settings specific to vanguard-core
 * @return {string} SQL statement ready to be executed against MySQL database.
 */
exports.saveSql = function (model, item, options) {
    // Start by determining if it is an insert or update
    if (item[model.id_key] && item[model.id_key] != 0) {
        // Must be an update
        insert = false;
        var updatesql = "update `{{model.database}}`.`{{model.table_name}}` set {{{values}}} where {{model.id_key}} = '{{id_key}}';";

        var temp = {
            model: model,
            valuesArray: [],
            values: '',
            id_key: item[model.id_key]
        };

        // Build columns list and values list
        _.each(_.filter(_.pairs(model.properties), function(p){return !p[1].fake;}), function (pair) {
            var name = pair[0];
            var propertyOptions = pair[1];
            var column_name = propertyOptions.column_name ? propertyOptions.column_name : pair[0];
            var value = typeof item[name] === 'undefined' ? "UNDEFINED-SKIP-THIS-FIELD" : item[name];
            var use_escapes = true;

            if (value === 'UNDEFINED-SKIP-THIS-FIELD') return;
            if (propertyOptions.type === 'datetime') {
                if (item[name] && item[name].toString().indexOf('1753') > -1) {
                    delete item[name];
                } else if (item[name]) {
                    value = (new Date(item[name])).toISOString().replace('T', ' ').replace('Z', '');
                    if (propertyOptions.trim_time) {
                        value = value.substr(0, 10) + ' 00:00:00';
                    }
                }
            }
            if (propertyOptions.type === 'json') {
                value = JSON.stringify(value);
            }
            if (propertyOptions.type === 'csv') {
                value = value.toString();
            }
            if (propertyOptions.type === 'boolean') {
                console.log(value);
                if (_.isBoolean(value)){
                    value = value === true ? 1 : 0;
                    use_escapes = false;
                } else if(_.isString(value)) {
                    value = value.toLowerCase() === 'true' ? 1 : 0;
                    use_escapes = false;
                } else {
                    value = value ? 1 : 0;
                    use_escapes = false;
                }
            }

            if (column_name.toLowerCase() !== model.id_key && typeof item[name] !== 'undefined') {
                // escape dangerous characters
                if (_.isString(value)) value = value.replaceAll("'", "''");
                if (value === null) {
                    temp.valuesArray.push("`" + column_name + "` = null");
                } else {
                    temp.valuesArray.push("`" + column_name + (use_escapes ? "` = '" + value + "'" : "` = " + value));
                }
            }

        });

        temp.values = temp.valuesArray.toString();
        sql = Mustache.render(updatesql, temp);

    } else {
        // Must be an insert
        var insertsql = "insert into `{{model.database}}`.`{{model.table_name}}` ({{{columns}}}) values ({{{values}}}); select Last_INSERT_ID()";

        var temp = {
            model: model,
            valuesArray: [],
            columnsArray: [],
            columns: '',
            values: ''
        };

        // Build columns list and values list
        _.each(_.filter(_.pairs(model.properties), function(p){return !p[1].fake;}), function (pair) {
            var name = pair[0];
            var propertyOptions = pair[1];
            var column_name = propertyOptions.column_name ? propertyOptions.column_name : pair[0];
            var value = item[name];
            var use_escapes = true;

            if (propertyOptions.type === 'datetime') {
                if (item[name] && item[name].toString().indexOf('1753') > -1) {
                    delete item[name];
                } else if (item[name]) {
                    value = (new Date(item[name])).toISOString().replace('T', ' ').replace('Z', '');
                    if (propertyOptions.trim_time) {
                        value = value.substr(0, 10) + ' 00:00:00';
                    }
                }
            }
            if (propertyOptions.type === 'json') {
                value = JSON.stringify(value);
            }
            if (propertyOptions.type === 'csv') {
                value = value.toString();
            }
            if (propertyOptions.type === 'boolean') {
                console.log('boolean');
                if (_.isBoolean(value)){
                    value = value === true ? 1 : 0;
                    use_escapes = false;
                } else if(_.isString(value)) {
                    value = value.toLowerCase() === 'true' ? 1 : 0;
                    use_escapes = false;
                } else {
                    value = value ? 1 : 0;
                    use_escapes = false;
                }
            }
            if (column_name.toLowerCase() !== model.id_key && typeof item[name] !== 'undefined') {
                // escape dangerous characters
                if (_.isString(value)) value = value.replaceAll("'", "''")
                temp.columnsArray.push("`" + column_name + "`");
                if (use_escapes) temp.valuesArray.push("'" + value + "'");
                else temp.valuesArray.push(value);
            }
        });

        temp.columns = temp.columnsArray.toString();
        temp.values = temp.valuesArray.toString();
        sql = Mustache.render(insertsql, temp);

    }
    return sql;
}

/*
 * Saves new record, or changes to existing record back to MySQL database.
 *
 * @param {object} model MVC model representing table record to add or update.
 * @param {object} item New or modified record to pass back to MySQL database.
 * @param {object} options Configuration settings and objects used for vanguard-core interaction with MySQL.
 * @param {object} callback Function to invoke after successful/failed SQL call to MySQL db. Check err for any errors that may have occurred, otherwise item will contain the record that was added/updated. New records will contain an id value in the id_key field.
 */
exports.save = function (model, item, options, callback) {
    var sql = exports.saveSql(model, item, options);
    var insert = !(item[model.id_key] && item[model.id_key] != 0);
    var close_conn = false;

    // Create the run function that is called when the connection is identified (either opened or passed on from outside)
    var run = function (conn) {
        conn.queryRaw(sql, function (err, results) {
            if (err) console.log({sql: sql, err: err});
            if (err) {
                callback(err, item);
            } else {
                // for node-mysql, insertId represents the unique identifier's newly assigned value.
                if (insert) item[model.id_key] = results[0]['insertid'];
                callback(undefined, item);
            }
            try {
                if (close_conn) conn.end();
            } catch (e) {
                console.log(e);
            }
        });
    };

    if (options.connection) {
        if (options.connection.state === 'disconnected') {
            vg.db.openConnection(model.database, function (err, conn) {
                if (err) {
                    callback(err, item);
                } else {
                    close_conn = true;
                    run(conn);
                }
            });
        } else {
            run(options.connection);
        }
    } else {
        vg.db.openConnection(model.database, function (err, conn) {
            if (err) {
                callback(err, item);
            } else {
                close_conn = true;
                run(conn);
            }
        });
    }
};

/*
 * Drops an existing table from MySQL database.
 *
 * @param {object} model MVC model object holding table schema to use for generating DROP DML.
 * @param {object} options Configuration settings and objects used for vanguard-core interaction with MySQL. NOTE: options also contains a callback property that upon successful/fail DROP will be invoked. Check err for any errors that may have occurred.
 */
exports.drop = function (model, options) {
    var sql = 'DROP TABLE if EXISTS ' + model.database + '.' + model.table_name;

    // Execute Sql for Table Sync
    vg.db.openConnection(model.database, function (err, conn) {
        if (err) console.log({sql: sql, err: err});
        if (err) console.log(err);
        conn.queryRaw(sql, function (err, results) {
            if (options.callback) options.callback(err);
        });
    });
};

/*
 * Wraps column name with appropriate characters to prevent keyword naming issues when executing sql queries.
 *
 * @param {string} name Column name to wrap.
 * @return {string} Wrapped column name.
 */
exports.formatColumn = function (name) {
    return "`" + name + "`";
};

/*
 * Primarily used for date fields. Will check value's type and based on type will apply the necessary formatting to the value.
 *
 * @param {object} value Field value to apply formatting to.
 * @param {string} type Field value's type.
 * return {object} Formatted value.
 */
exports.formatValue = function (value, type) {
    if (type) {
        switch (type.toLowerCase()) {
            case "date":
                var d = new Date(value);
                d.setHours(0);
                d.setMinutes(0);
                d.setSeconds(0);
                d.setMilliseconds(0);
                var s = d.toISOString();
                s = s.replace('01:', '00:');
                s = s.replace('02:', '00:');
                s = s.replace('03:', '00:');
                s = s.replace('04:', '00:');
                s = s.replace('05:', '00:');
                s = s.replace('06:', '00:');
                s = s.replace('07:', '00:');
                s = s.replace('08:', '00:');
                s = s.replace('09:', '00:');
                s = s.replace('10:', '00:');
                s = s.replace('11:', '00:');
                s = s.replace('12:', '00:');
                s = s.replace('13:', '00:');
                s = s.replace('T00:00:00.000Z', '');
                return s;
                break;
            case "datetime":
                return new Date(value).toISOString().replace('T', ' ').replace('Z', ' ')
                break;
        }
    }
    return value;
};

/*
 * Retrieves the table alias to use in SQL Select and other DML statements.
 *
 * @param {string} db name of current MySQL database instance.
 * @param {string} table Name of MySQL database table.
 * return name of alias to use, or (if not found) just the table name.
 */
exports.formatTable = function (db, table) {
    var map = vg.db[db].map;
    if (map[table] && map[table].alias) {
        return map[table].alias;
    } else {
        return table;
    }
};

/*
 * Takes a model class and returns it with a formatted set of properties (table_name, schema, database, etc.)
 *
 * @param {object} m MVC model object to format using vanguard-core
 * @return formatted vanguard model object.
 */
exports.formatter = function (m) {
    var model = _.clone(m);
    var db = model.database;
    model.database = "`" + db + "`";
    model.schema = "`" + model.schema + "`";
    model.map = vg.db[db].map;
    model.id_key = model.formatColumn(model.id_key);

    _.each(_.pairs(model.properties), function (pair) {
        var name = pair[0];
        var propertyOptions = pair[1];
        pair[1].formatted = model.formatColumn(pair[1].column_name);

    });

    model.formatValue = function () {
        return function (text, render) {
            return m.formatValue(text);
        };
    };

    return model;
};

exports.query_procedure = function(model, sql, qSettings, callback) {
    if (_.isFunction(qSettings)) {
        var callback = qSettings;
        qSettings = {};
    }
    qSettings = qSettings ? qSettings : {};
    qSettings.stored_procedure = true;
    exports.query(model, sql, qSettings, callback);
}

/*
 * Executes a SQL query against MySQL.
 *
 * @param {object} model Vanguard MVC model. Used in preparation of SQL statement to execute against MySQL.
 * @param {sql} sql SQL statement to be properly formatted to work correctly against MySQL database.
 * @param {object} Query settings to apply to sql statement. Also holds connection object to be used for executing SQL query.
 * @callback {object} Callback Function to invoke upon successful/fail query. Check err for any errors that may have occurred, otherwise, 2nd argument will contain an array of models represents results returned from database.
 */
exports.query = function (model, sql, qSettings, callback) {

    // Adjust SQL to replace any special keywords with the proper values (database, schema, etc)
    var formattedModel = exports.formatter(model);
    sql = Mustache.render(sql, formattedModel);

    if (vg._.isFunction(qSettings)) {
        var callback = qSettings;
        qSettings = _.clone(model.defaultQuerySettings);
    }

    if (qSettings.cache_only) {
        var cache_columns = [];
        _.each(_.pairs(model.properties), function (pair) {
            var options = pair[1];
            var column_name = options.column_name ? options.column_name : pair[0];
            if (options.cached) {
                cache_columns.push(exports.formatColumn(column_name));
            }
        });
        if (cache_columns.length === 0 || cache_columns.indexOf(model.id_key) === -1)
            cache_columns.push(exports.formatColumn(model.id_key));
        sql = sql.toLowerCase().replace('select * ', 'select ' + cache_columns.toString() + ' ');
        sql = sql.toLowerCase().replace('select * ', 'select ' + cache_columns.toString() + ' ');
    }

    var qResults = [];
    vg.flow.exec(

        function () {
            var next = this;
            // if no settings.connection then open one
            if (!qSettings.connection) {
                vg.db.openConnection(model.database, next);
                qSettings.close_conn = true;
            } else {
                // double check existing connection object. create a new one if we're disconnected from MySQL
                if (qSettings.connection.state === 'disconnected') {
                    vg.db.openConnection(model.database, next);
                    qSettings.close_conn = true;
                } else {
                    next(undefined, qSettings.connection);
                }
            }

        }, function (err, conn) {
            qSettings.connection = conn;
            var next = this;
            // if err then bail
            if (err) {
                callback(err, []);
            } else {
                // begin query
                qSettings.connection.queryRaw(sql, next);
            }
        }, function (err, results) {
            //console.log('Query Returned');
            //console.log('Rows:' + results.rows.length);
            try {
                if (qSettings.close_conn) {
                    qSettings.connection.end();
                }
            } catch (e) {
                console.log(e);
            }
            if (err) console.log({sql: sql, err: err});
            var next = this;
            // if err then bail
            if (err) {
                callback(err, []);
            } else {
                results = qSettings.stored_procedure ? results[0] : results
                callback(undefined, model.mold(results));
            }
        });

};

/*
 * Synchronizes model existence with MySQL database. In the event that the database table does not exist, it will be created based off of the model's schema.
 *
 * @param {object} model Vanguard MVC model to use for checking MySQL for table existence. Will also be used when preparing DML statements.
 * @param {object} options Configuration settings to be used to apply to MySQL connections. Callback function is attached to this object as well, called upon successful/failed sync. Check err for any errors that may have occurred.
 */
exports.sync = function (model, options) {

    var sql = "CREATE TABLE IF NOT EXISTS `{0}`.`{1}` ( {2} );";
    var columns = [];

    // Build Create Table Statement for this model
    _.each(_.pairs(model.properties), function (pair) {
        var colsql = "";
        var propertyOptions = pair[1];
        var name = propertyOptions.column_name ? propertyOptions.column_name : pair[0];
        colsql += "`" + name.toLowerCase() + "`";

        // Handle Create Table Mapping
        switch (propertyOptions.type.toLowerCase()) {
            case 'key':
                colsql += ' INT(11) AUTO_INCREMENT';
                columns.push('PRIMARY KEY (`' + name + '`)')
                break;
            case 'integer':
            case 'int':
            case 'number':
                colsql += ' INT(11)';
                break;
            case 'string':
                colsql += ' VARCHAR(' + (propertyOptions.limit ? propertyOptions.limit : '21844') + ')';
                break;
            case 'text':
                colsql += ' TEXT';
                break;
            case 'binary':
                colsql += ' BLOB';
                break;
            case 'decimal':
                var precision = propertyOptions.precision ? propertyOptions.precision : '18';
                var scale = propertyOptions.scale ? propertyOptions.scale : '2';
                colsql += ' DECIMAL(' + precision + ',' + scale + ')';
                break;
            case 'json':
                colsql += ' TEXT';
                break;
            case 'csv':
                colsql += ' TEXT';
                break;
            case 'datetime':
                colsql += ' DATETIME';
                break;
        }

        // Handle Not Null Option
        if (propertyOptions.notNull)
            colsql += ' NOT NULL';
        else
            colsql += ' DEFAULT NULL';
        columns.push(colsql);

    });

    sql = sql.format(model.database, model.table_name, columns.toString());

    //console.log(sql);
    // Execute Sql for Table Sync
    vg.db.openConnection(model.database, function (err, conn) {
        if (err)
            console.log(err);

        //console.log(sql);
        conn.query(sql, function (err, rows, fields) {
            //console.log(results);
            if (options.callback)
                options.callback(err);
        });
    });
};