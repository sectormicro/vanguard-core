var Connection = require('tedious').Connection;
var Request = require('tedious').Request;

exports.openConnection = function (info, callback) {
    info.options = info.options ? info.options : {};
    info.options.rowCollectionOnRequestCompletion = true;
    var connection = new Connection(info);
    connection.on('connect', function (err) {
        //console.log('Connection Established');
        if (connection) {
            connection.queryRaw = exports.queryRaw;
            connection.queryRawBatch = exports.queryRawBatch;
        }
        callback(err, connection);
    });
}

exports.queryRaw = function (sql, callback) {
    //console.log('Raw Query: ' + sql);
    var request = new Request(sql, function (err, count, rows) {
        if (err) console.log(err);
        var data = [];
        _.each(rows, function (row) {
            var entry = {};
            _.each(row, function (field) {
                entry[field.metadata.colName.toLowerCase()] = field.value;
            })
            data.push(entry);
        });
        callback(err, data);
    });
    this.execSql(request);
};

exports.queryBatch = function (sql, settings, callback) {
    var payload = [];
    var adapter = this;
    flow.exec(function () {
            if (!settings.connection) {
                adapter.openConnection(adapter.connection_info, this);
            } else {
                this(null, settings.connection);
            }
        },
        function (err, connection) {
            connection.queryRawBatch(sql, function (err, set) {
                if (!set || err) {
                    callback(err, payload);
                } else if (settings.models && set.length != settings.models.length)
                    callback('Query returned with more sets than expected');
                else {
                    if (settings.sets) {
                        payload = {};
                        _.each(settings.sets, function (set_item, i) {
                            payload[set_item.name] = exports.mold(set_item.model, set[i]);
                        });
                    } else {
                        _.each(settings.models, function (model, i) {
                            payload.push(exports.mold(model, set[i]));
                        });
                    }
                    callback(err, payload);
                }
            });
        });
};

exports.saveCollection = function (collection, settings, callback) {

    var sql = '';

    _.each(collection, function (item) {

        sql += exports.saveSql(settings.model, item, {});

    });

    settings.connection.queryRawBatch(sql, function (err, set) {

        _.each(collection, function (item, i) {

            if (!(item[settings.model.id_key] && item[settings.model.id_key] != 0)) {
                try {
                    item[settings.model.id_key] = set[i][0][settings.model.id_key];
                } catch (e) {
                    console.log(e);
                }
            }

        });

        callback(err, collection);

    });

};

exports.queryRawBatch = function (sql, callback) {
    var conn = this;
    //console.log('Raw Query: ' + sql);
    var request = new Request(sql, function (err, count) {
        if (err) callback(err);
        callback(null, set);
    });
    var set = [];
    var offset = -1;
    request.on('done', function (count, more, rows) {

        var d = [];
        _.each(rows, function (row, i) {
            if (i >= offset) {
                var entry = {};
                _.each(row, function (field) {
                    entry[field.metadata.colName.toLowerCase()] = field.value;
                })
                d.push(entry);
            }
        });
        offset = rows.length;
        set.push(d);
        if (!more) {
            //callback(null, set);
        }
    });
    //console.log(conn);
    conn.execSqlBatch(request);
};

exports.open_begin = function (info, callback) {
    info.options = info.options ? info.options : {};
    info.options.rowCollectionOnRequestCompletion = true;
    var connection = new Connection(info);
    connection.on('connect', function (err) {
        if (err)
            callback(err);
        else {
            connection.queryRaw = exports.queryRaw;
            connection.queryRawBatch = exports.queryRawBatch;
            connection.beginTransaction(function (err) {
                callback(err, connection);
            });
        }
    });
}

exports.select_cache = function (model) {
    var cache_columns = [];
    _.each(_.pairs(model.properties), function (pair) {
        var options = pair[1];
        var column_name = options.column_name ? options.column_name : pair[0];
        if (options.cached) {
            cache_columns.push(exports.formatColumn(column_name));
        }
    });
    if (cache_columns.length === 0 || cache_columns.indexOf(model.id_key) === -1)
        cache_columns.push(exports.formatColumn(model.id_key));
    return cache_columns.toString();
};

exports.buildController = function (model_name, db, table_name, callback) {
    exports.openConnection(vg.config.databases[db], function (err, conn) {
        var sql = 'select SchemaName = schema_name(t.schema_id) , TableName = t.name , ColumnName = c.name , IsPrimaryKey = (select count(*) from   sys.indexes as i join sys.index_columns as ic on i.OBJECT_ID = ic.OBJECT_ID and i.index_id = ic.index_id and ic.column_id = c.column_id   where i.is_primary_key = 1 and i.object_id = t.object_id) , IsIdentity = c.is_identity , ColumnType = TYPE_NAME(c.system_type_id) , IsNullable = c.is_nullable , Precision = c.precision , Scale = c.scale , MaxLength = c.max_length from sys.tables t join sys.columns c on t.object_id = c.object_id where t.name = \'' + table_name + '\' order by t.name , c.column_id ;';
        //console.log(sql);
        conn.queryRaw(sql, function (err, results) {
            try {
                if (err) throw new Error(err);
                var properties = [];
                var model = {model_name: model_name};
                _.each(results, function (row) {
                    var column_name = row.columnname.toLowerCase().replace('#', '_number');
                    var type = row.columntype;
                    var precision = row.precision;
                    var scale = row.scale;
                    var nullable = row.isnullable.toString().toLowerCase() === 'true' ? true : false;
                    var maxlength = row.maxlength;
                    var identity = row.isidentity.toString().toLowerCase() === 'true' ? true : false;

                    if (identity)
                        model.id_key = column_name;

                    var property = {
                        property_name: column_name,
                        column_name: column_name,
                        max_length: maxlength,
                        type: identity ? 'key' : type,
                        not_null: nullable ? false : false
                    };

                    switch (type.toLowerCase()) {
                        case 'varchar':
                            property.type = 'string';
                            break;
                    }
                    properties.push(property);
                });


                model.properties = properties;
                model.table_name = table_name;
                model.database = vg.config.databases[db].database;
                model.schema = vg.config.databases[db].schema;
                model.model_name_lower = model_name.toLowerCase();
                model.today = new Date().toString();

                var content = Mustache.render(fs.readFileSync(__dirname + '\\..\\tmpl\\controller.js.mustache', 'utf-8'), model);

                fs.writeFileSync(process.cwd() + '\\' + model_name.toLowerCase() + '_controller.js', content);

                callback(null, model);

            } catch (err) {
                callback(err, false);
            }
        });
    });
}

exports.buildModel = function (model_name, db, table_name, callback) {
    exports.openConnection(vg.config.databases[db], function (err, conn) {
        var sql = 'select SchemaName = schema_name(t.schema_id) , TableName = t.name , ColumnName = c.name , IsPrimaryKey = (select count(*) from   sys.indexes as i join sys.index_columns as ic on i.OBJECT_ID = ic.OBJECT_ID and i.index_id = ic.index_id and ic.column_id = c.column_id   where i.is_primary_key = 1 and i.object_id = t.object_id) , IsIdentity = c.is_identity , ColumnType = TYPE_NAME(c.system_type_id) , IsNullable = c.is_nullable , Precision = c.precision , Scale = c.scale , MaxLength = c.max_length from sys.tables t join sys.columns c on t.object_id = c.object_id where t.name = \'' + table_name + '\' order by t.name , c.column_id ;';
        //console.log(sql);
        conn.queryRaw(sql, function (err, results) {
            try {
                if (err) throw new Error(err);
                var properties = [];
                var model = {model_name: model_name};
                _.each(results, function (row) {
                    var column_name = row.columnname.toLowerCase().replace('#', '_number');
                    var type = row.columntype;
                    var precision = row.precision;
                    var scale = row.scale;
                    var nullable = row.isnullable.toString().toLowerCase() === 'true' ? true : false;
                    var maxlength = row.maxlength;
                    var identity = row.isidentity.toString().toLowerCase() === 'true' ? true : false;

                    if (identity)
                        model.id_key = column_name;

                    var property = {
                        property_name: column_name,
                        column_name: column_name,
                        type: identity ? 'key' : type,
                        not_null: nullable ? false : false
                    };

                    switch (type.toLowerCase()) {
                        case 'varchar':
                            property.type = 'string';
                            break;
                    }
                    properties.push(property);
                });


                model.properties = properties;
                model.table_name = table_name;
                model.database = vg.config.databases[db].database;
                model.schema = vg.config.databases[db].schema;


                var content = Mustache.render(fs.readFileSync(__dirname + '/../tmpl/model.js.mustache', 'utf-8'), model);

                fs.writeFileSync('./' + model_name.toLowerCase() + '_model.js', content);

                callback(null, model);

            } catch (err) {
                callback(err, false);
            }
        });
    });
};

exports.mold = function (model, results) {
    var items = [];
    if (model !== 'raw') {
        _.each(results, function (row) {
            var qrow = {
                _raw: row,
                raw: row
            };

            // Iterate through each property
            _.each(_.pairs(model.properties), function (pair) {

                var propertyColumn = pair[1].column_name ? pair[1].column_name : pair[0];
                qrow[pair[0]] = row[propertyColumn.toLowerCase()];

            });

            model._attachInstanceFunctions(qrow);
            qrow.model = model;
            qrow._original = _.clone(qrow);
            items.push(qrow);

        });
    } else {
        items = results;
    }
    return items;
};

exports.saveSql = function (model, item, options) {
    // Start by determining if it is an insert or update

    if (item[model.id_key] && item[model.id_key] != 0) {

        // Must be an update

        insert = false;

        var updatesql = "update [{{model.database}}].[{{model.schema}}].[{{model.table_name}}] set {{{values}}} where {{model.id_key}} = '{{id_key}}';";

        var temp = {
            model: model,
            valuesArray: [],
            values: '',
            id_key: item[model.id_key]
        };

        // Build columns list and values list

        _.each(_.pairs(model.properties), function (pair) {

            var name = pair[0];
            var propertyOptions = pair[1];
            var column_name = propertyOptions.column_name ? propertyOptions.column_name : pair[0];
            var value = typeof item[name] === 'undefined' ? "UNDEFINED-SKIP-THIS-FIELD" : item[name];
            if (value === 'UNDEFINED-SKIP-THIS-FIELD') return;
            if (propertyOptions.type === 'datetime') {
                if (item[name] && item[name].toString().indexOf('1753') > -1) {
                    delete item[name];
                } else if (item[name]) {
                    value = (new Date(item[name])).toISOString().replace('T', ' ').replace('Z', '');
                    if (propertyOptions.trim_time) {
                        value = value.substr(0, 10) + ' 00:00:00';
                    }
                }
            }

            if (column_name.toLowerCase() !== model.id_key) {

                // escape dangerous characters
                if (_.isString(value)) value = value.replaceAll("'", "''")

                if (value === null) {
                    temp.valuesArray.push("[" + column_name + "] = null");
                } else {
                    temp.valuesArray.push("[" + column_name + "] = '" + value + "'");
                }
            }

        });

        temp.values = temp.valuesArray.toString();

        sql = Mustache.render(updatesql, temp);

    } else {

        // Must be an insert

        var insertsql = "insert into [{{model.database}}].[{{model.schema}}].[{{model.table_name}}] ({{{columns}}}) OUTPUT INSERTED.{{{model.id_key}}} values ({{{values}}});";

        var temp = {
            model: model,
            valuesArray: [],
            columnsArray: [],
            columns: '',
            values: ''
        };

        // Build columns list and values list

        _.each(_.pairs(model.properties), function (pair) {

            var name = pair[0];
            var propertyOptions = pair[1];
            var column_name = propertyOptions.column_name ? propertyOptions.column_name : pair[0];
            var value = item[name];

            if (propertyOptions.type === 'datetime') {
                if (item[name] && item[name].toString().indexOf('1753') > -1) {
                    delete item[name];
                } else if (item[name]) {
                    value = (new Date(item[name])).toISOString().replace('T', ' ').replace('Z', '');
                    if (propertyOptions.trim_time) {
                        value = value.substr(0, 10) + ' 00:00:00';
                    }
                }
            }

            if (column_name.toLowerCase() !== model.id_key && item[name]) {
                // escape dangerous characters
                if (_.isString(value)) value = value.replaceAll("'", "''")
                temp.columnsArray.push("[" + column_name + "]");
                temp.valuesArray.push("'" + value + "'");
            }

        });

        temp.columns = temp.columnsArray.toString();
        temp.values = temp.valuesArray.toString();

        sql = Mustache.render(insertsql, temp);

    }
    return sql;
}

exports.save = function (model, item, options, callback) {
    //console.log('SAVING');
    //console.log('-------');
    //console.log(item);
    var sql = exports.saveSql(model, item, options);
    var insert = !(item[model.id_key] && item[model.id_key] != 0);


    //console.log(sql);

    var close_conn = false;

    // Create the run function that is called when the connection is identified (either opened or passed on from outside)
    var run = function (conn) {
        conn.queryRaw(sql, function (err, results) {
            if (err) console.log({sql: sql, err: err});
            //console.log(results);
            if (err) {
                callback(err, item);
            } else {
                if (insert) item[model.id_key] = results[0][model.id_key];
                callback(null, item);
            }
            try {
                if (close_conn) conn.close();
            } catch (e) {
                console.log(e);
            }
        });
    };

    if (options.connection) {

        run(options.connection);

    } else {
        vg.db.openConnection(model.database, function (err, conn) {

            if (err) {
                callback(err, item);
            } else {
                close_conn = true;
                run(conn);
            }

        });

    }
}

exports.drop = function (model, options) {
    var sql = "IF object_id(N'[{2}].[{0}].[{1}]') is not null DROP TABLE [{2}].[{0}].[{1}];"

    sql = sql.format(model.schema, model.table_name, model.database);


    // Execute Sql for Table Sync
    vg.db.openConnection(model.database, function (err, conn) {
        if (err) console.log(err);

        ////console.log(sql);
        conn.queryRaw(sql, function (err, results) {
            ////console.log(results);
            if (options.callback) options.callback(err);
        });
    });
}

// @formatColumn
exports.formatColumn = function (name) {
    return "[" + name + "]";
};

exports.formatValue = function (value, type) {
    if (type) {
        switch (type.toLowerCase()) {
            case "date":
                var d = new Date(value);
                d.setHours(0);
                d.setMinutes(0);
                d.setSeconds(0);
                d.setMilliseconds(0);
                var s = d.toISOString();
                s = s.replace('01:', '00:');
                s = s.replace('02:', '00:');
                s = s.replace('03:', '00:');
                s = s.replace('04:', '00:');
                s = s.replace('05:', '00:');
                s = s.replace('06:', '00:');
                s = s.replace('07:', '00:');
                s = s.replace('08:', '00:');
                s = s.replace('09:', '00:');
                s = s.replace('10:', '00:');
                s = s.replace('11:', '00:');
                s = s.replace('12:', '00:');
                s = s.replace('13:', '00:');
                s = s.replace('T00:00:00.000Z', '');
                return s;
                break;
            case "datetime":
                return new Date(value).toISOString().replace('T', ' ').replace('Z', ' ')
                break;
        }
    }
    return value;
};

exports.formatTable = function(db, table) {
    var map = vg.db[db].map;
    if (map[table] && map[table].alias) {
        return map[table].alias;
    } else {
        return table;
    }
};

// @formatter
// Takes a model class and returns it with a formatted set of properties (table_name, schema, database, etc.)
exports.formatter = function (m) {
    var model = _.clone(m);
    var db = model.database;
    model.database = "[" + db + "]";
    model.schema = "[" + model.schema + "]";
    model.map = vg.db[db].map;
    model.id_key = model.formatColumn(model.id_key);

    _.each(_.pairs(model.properties), function (pair) {

        var name = pair[0];
        var propertyOptions = pair[1];

        pair[1].formatted = model.formatColumn(pair[1].column_name);

    });

    model.formatValue = function () {

        return function (text, render) {
            return m.formatValue(text);
        };

    };

    return model;
};

//  @query(model, sql, settings, callback)
//	general querying method
//
exports.query = function (model, sql, qSettings, callback) {


    // Adjust SQL to replace any special keywords with the proper values (database, schema, etc)
    var formattedModel = model.formatter(model);
    sql = Mustache.render(sql, formattedModel);

    if (vg._.isFunction(qSettings)) {
        var callback = qSettings;
        qSettings = model.defaultQuerySettings;
    }

    if (qSettings.cache_only) {
        var cache_columns = [];
        _.each(_.pairs(model.properties), function (pair) {
            var options = pair[1];
            var column_name = options.column_name ? options.column_name : pair[0];
            if (options.cached) {
                cache_columns.push(exports.formatColumn(column_name));
            }
        });
        if (cache_columns.length === 0 || cache_columns.indexOf(model.id_key) === -1)
            cache_columns.push(exports.formatColumn(model.id_key));
        sql = sql.toLowerCase().replace('select * ', 'select ' + cache_columns.toString() + ' ');
    }

    var qResults = [];
    vg.flow.exec(

        function () {
            var next = this;
            // if no settings.connection then open one
            if (!qSettings.connection) {
                vg.db.openConnection(model.database, next);
                qSettings.close_conn = true;
            } else {
                next(null, qSettings.connection);
            }

        }, function (err, conn) {
            qSettings.connection = conn;
            var next = this;
            // if err then bail
            if (err) {
                callback(err, []);
            } else {
                // begin query
                qSettings.connection.queryRaw(sql, next);
            }
        }, function (err, results) {
            //console.log('Query Returned');
            //console.log('Rows:' + results.rows.length);
            try {
                if (qSettings.close_conn) {
                    qSettings.connection.close();
                    delete qSettings.connection;
                }
            } catch (e) {
                console.log(e);
            }
            if (err) console.log({sql: sql, err: err});
            var next = this;
            // if err then bail
            if (err) {
                callback(err, []);
            } else {
                callback(null, model.mold(results));
            }
        });

}

exports.sync = function (model, options) {

    var sql = "IF object_id(N'[{3}].[{0}].[{1}]') is null CREATE TABLE [{3}].[{0}].[{1}] ( {2} );"
    var colsql = "";
    var key = '';
    // Build Create Table Statement for this model
    _.each(_.pairs(model.properties), function (pair) {

        var propertyOptions = pair[1];
        var name = propertyOptions.column_name ? propertyOptions.column_name : pair[0];
        colsql += name.toLowerCase();

        // Handle Create Table Mapping
        switch (propertyOptions.type.toLowerCase()) {
            case 'key':
                colsql += ' INT IDENTITY(1,1)';
                key = name.toString();
                break;
            case 'integer':
            case 'int':
            case 'number':
                colsql += ' INT';
                break;
            case 'string':
                colsql += ' VARCHAR(' + (propertyOptions.limit ? propertyOptions.limit : 'MAX') + ')';
                break;
            case 'text':
                colsql += ' TEXT';
                break;
            case 'binary':
                colsql += ' VARBINARY(MAX)';
                break;
            case 'decimal':
                var precision = propertyOptions.precision ? propertyOptions.precision : '18';
                var scale = propertyOptions.scale ? propertyOptions.scale : '2';
                colsql += ' DECIMAL(' + precision + ',' + scale + ')';
                break;
            case 'datetime':
                colsql += ' DATETIME';
                break;
        }

        // Handle Not Null Option
        if (propertyOptions.notNull) colsql += ' NOT NULL,';
        else colsql += ' ,';

    });


    if (key) {
        colsql += ' PRIMARY KEY ("' + key + '")';
    }

    sql = sql.format(model.schema, model.table_name, colsql, model.database);


    // Execute Sql for Table Sync
    vg.db.openConnection(model.database, function (err, conn) {
        if (err)
            console.log(err);

        ////console.log(sql);
        conn.queryRaw(sql, function (err, results) {
            ////console.log(results);
            if (options.callback) options.callback(err);
        });
    });
}