vg.utils = vg.utils ? vg.utils : {};
vg.utils.crypto = vg.utils.crypto ? vg.utils.crypto : {};

// Module inclusions
vg.utils.crypto.RC4 = require('./encryption/rc4');
vg.utils.crypto.legacy = require('./encryption/legacy');

// @String.prototype.format 
// Used to format strings in the style of c#
if (!String.prototype.format) {
    String.prototype.format = function () {
        var args = arguments;
        return this.replace(/{(\d+)}/g, function (match, number) {
            return typeof args[number] != 'undefined'
                ? args[number]
                : match
                ;
        });
    };
}

if (!String.prototype.replaceAll) {
    String.prototype.replaceAll = function (target, replacement) {
        return this.split(target).join(replacement);
    };
}


Date.prototype.toGMT = function(){
    var d = new Date(this);
    d.setMinutes(d.getMinutes() + d.getTimezoneOffset());
    return d;
}
Date.prototype.toLocal = function(){
    var d = new Date(this);
    d.setMinutes(d.getMinutes() - d.getTimezoneOffset());
    return d;
}