exports.model = require('./model');

exports._ = require('underscore');

exports.flow = require('flow');

exports.config = {};

exports.db = {};


// @openConnection(db, callback)
// Opens a connection for the specified db and passes it to the callback(err, conn)
exports.db.openConnection = function(db, callback) {
	exports.db.getAdapter(db).openConnection(exports.config.databases[db], callback);
}

exports.db.getAdapter = function(db) {
	var adapter;
	switch (exports.config.databases[db].adapter.toLowerCase()) {
        case 'mstds':
            adapter = require('./adapters/mstds');
            break;
		case 'mysql':
			adapter = require('./adapters/mysql');
		break;
	}
    adapter.db = db;
    adapter.connection_info = vg.config.databases[db];
	return adapter;
}

exports.debug = true;


// GLOBALS SETUP

global._ = require('underscore');
global.Mustache = require('mustache');
global.fs = require('fs');
global.flow = require('flow');
global.exec = require('child_process').exec;