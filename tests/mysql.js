/**
 * Created by James on 1/9/14.
 */
var should = require('should');
// need to set up vanguard to run outside of a node app server environment.
var vgApp = require('../app.js');
if (!process.env.db) console.log('WARNING: Missing environment variable "db"');
if (!process.env.env) console.log('WARNING: Missing environment variable "env"');
var sectorenv = (require('../environments/' + process.env.env));

// do not need express server running for our mocha tests
sectorenv.disableWebServer = true;
vgApp.initialize(sectorenv);

global.vg = require('../lib/core');
global.vg.config = sectorenv;


describe('Model', function () {
    describe('#new', function () {
        it('should create a new model', function () {
            //console.log('~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~');
            Agent = vg.model.define('Agent', {
                table_name: 'AGENT',
                id_key: 'recnum',
                database: process.env.db,
                properties: {
                    recnum: {type: 'key'},
                    first_name: {type: 'string', limit: 45},
                    createdAt: {type: 'datetime'},
                    cash_on_hand: {type: 'decimal'},
                    json_test: {type: 'json'},
                    csv_test: {type: 'csv'}
                },
                instanceFunctions: {
                    greet: function () {
                        return "Hello my name is " + this.first_name;
                    }
                },
                classFunctions: {
                    greet: function () {
                        return "This is the Agent Model Class";
                    }
                }
            });
            should.exist(Agent);
        });
    });

    describe('#sync', function () {
        it('should sync this model\'s schema into the db', function (done) {
            //console.log('Beginning SYNC Test!');
            Agent.sync({
                callback: function (err) {
                    if (err) console.log(err);
                    should.not.exist(err);
                    done();
                }
            });
        });
    });


    describe('#create', function () {
        it('should create and save an instance for this model', function (done) {
            //console.log('Creating Insance of Model');
            var agent = Agent.create({
                first_name: 'Timmy',
                createdAt: new Date(),
                cash_on_hand: 456.83,
                json_test: {height: '6 foot 11 inches', weight: 265},
                csv_test: [56, 67]
            });
            should.exist(agent.greet());
            should.exist(agent.model);
            agent.save({}, function (err, a) {
                should.not.exist(err);
                should.exist(a.recnum);
                done();
            });
        });
    });

    describe('#update', function () {
        it('should update an instance for this model', function (done) {
            //console.log('Updating Insance of Model');

            vg.db.getAdapter(process.env.db).open_begin(vg.config.databases[process.env.db], function (err, conn) {
                should.not.exist(err);
                var connection = conn;
                var agent = Agent.query('select * from {{database}}.{{table_name}} order by {{id_key}} desc', function (err, data) {
                    should.exist(connection);
                    should.not.exist(err);
                    should.exist(data);
                    if (err) console.log(err);
                    data[0].first_name = 'Johnny';
                    data[0].csv_test.push(999);
                    data[0].json_test.age = 99;
                    data[0].save({connection: connection}, function (err, agent2) {
                        //console.log('Save Returned');
                        should.not.exist(err);
                        connection.commit(function(err){
                            should.not.exist(err);
                            done();
                        })
                    });
                });

            });
        })
    });

    describe('#sync', function () {
        it('should drop this model\'s table from the db', function (done) {
            //console.log('Beginning Drop Test!');
            Agent.drop({
                callback: function (err) {
                    if (err) console.log(err);
                    should.not.exist(err);
                    done();
                }
            });
        });
    });
});
