global.vg = require('../lib/core');
var should = require('should');


vg.config.databases = {
    'SQL29I': {
        adapter: 'mstds',
        schema: 'dbo',
        name: 'SQL29I',
        options: {
            database: 'SQL29I'
        },
        userName: 'sa',
        password: 'smc399',
        server: '192.168.2.5'
    }
};

/*
 descript('Database Initialization', function(){
 describe('#query', function(){
 it('should create a new database', function(done){

 });
 });
 });
 */

describe('MSSQL Adapter', function () {
    describe('#build_model', function () {
        it('Should output a model file and return true.', function (done) {
            vg.db.getAdapter('SQL29I').buildController('Customer', 'SQL29I', 'CUSTOMER', function (err, result) {
                console.log(err);
                should.not.exist(err);
                done();
            });
        });
    })
});