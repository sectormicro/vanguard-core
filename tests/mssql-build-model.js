global.vg = require('../lib/core');
var should = require('should');


vg.config.databases = {
  'SQL29I': {
      adapter: 'mssql',
    schema: 'dbo',
    database: 'SQL29I',
    connection_string: 'Driver={SQL Server Native Client 11.0};Database=SQL29I;Server=192.168.2.5;Uid=sa;Pwd=smc399;'
  },
}

/*
descript('Database Initialization', function(){
  describe('#query', function(){
    it('should create a new database', function(done){

    });
  });
});
*/

describe('MSSQL Adapter', function(){
  describe('#build_model', function(){
    it('Should output a model file and return true.', function(done){
      vg.db.getAdapter('SQL29I').buildModel('Customer','SQL29I','CUSTOMER', function(err, result){
        should.not.exist(err);
        done();
      });
    });
  })
});