global.vg = require('../lib/core');
var should = require('should');

var Agent = null;

vg.config.databases = {
  'vanguard_sandbox': {
      adapter: 'mssql',
    connection_string: 'Driver={SQL Server Native Client 11.0};Server=192.168.2.5;Uid=sa;Pwd=smc399;'
  },
}

/*
descript('Database Initialization', function(){
  describe('#query', function(){
    it('should create a new database', function(done){

    });
  });
});
*/

describe('Model', function(){
  describe('#new', function(){
    it('should create a new model', function(){
      //console.log('~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~');
      Agent = vg.model.define('Agent', {
        table_name: 'AGENT',
        schema: 'dbo',
        id_key: 'recnum',
        database: 'vanguard_sandbox',
        properties: {
          recnum: {type:'key'},
          first_name: {type:'string'},
          createdAt: {type: 'datetime'},
          cash_on_hand: {type: 'decimal'}
        },
        instanceFunctions: {
          greet: function() {
            return "Hello my name is " + this.first_name;
          }
        },
        classFunctions: {
          greet: function() {
            return "This is the Agent Model Class";
          }
        }
      });
      should.exist(Agent);
    })
  })

  describe('#sync', function(){
    it('should sync this model\'s schema into the db', function(done){
      //console.log('Beginning SYNC Test!');
      Agent.sync({
        callback: function(err){
          console.log(err);
          should.not.exist(err);
          done();
        }
      });
    })
  })


  describe('#create', function(){
    it('should create and save an instance for this model', function(done){
      //console.log('Creating Insance of Model');
      var agent = Agent.create({
        first_name: 'Timmy',
        createdAt: new Date(),
        cash_on_hand: 456.83
      });
      should.exist(agent.greet());
      should.exist(agent.model);
      agent.save({},function(err, a){
        should.not.exist(err);
        should.exist(a.recnum);
        done();
      });
    })
  });

  describe('#update', function(){
    it('should update an instance for this model', function(done){
      //console.log('Updating Insance of Model');
      var agent = Agent.query('select * from {{database}}.{{schema}}.{{table_name}} order by {{id_key}} desc', function(err, data){
        should.not.exist(err);
        if (err) console.log(err);
        data[0].first_name = 'Johnny';
        data[0].save({},function(err, agent2){
          //console.log('Save Returned');
          should.not.exist(err);
          done();
        });
      });
    })
  });

  describe('#sync', function(){
    it('should drop this model\'s table from the db', function(done){
      //console.log('Beginning Drop Test!');
      Agent.drop({
        callback: function(err){
          if (err) console.log(err);
          should.not.exist(err);
          done();
        }
      });
    })
  })
})
